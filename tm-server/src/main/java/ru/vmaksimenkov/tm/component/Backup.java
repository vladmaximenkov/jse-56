package ru.vmaksimenkov.tm.component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import ru.vmaksimenkov.tm.api.service.IAuthService;
import ru.vmaksimenkov.tm.api.service.dto.IProjectRecordService;
import ru.vmaksimenkov.tm.api.service.dto.ISessionRecordService;
import ru.vmaksimenkov.tm.api.service.dto.ITaskRecordService;
import ru.vmaksimenkov.tm.api.service.dto.IUserRecordService;
import ru.vmaksimenkov.tm.dto.Domain;
import ru.vmaksimenkov.tm.dto.ProjectRecord;
import ru.vmaksimenkov.tm.dto.TaskRecord;
import ru.vmaksimenkov.tm.dto.UserRecord;
import ru.vmaksimenkov.tm.service.PropertyService;

import java.io.File;
import java.io.FileOutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import static ru.vmaksimenkov.tm.constant.PropertyConst.BACKUP_XML;
import static ru.vmaksimenkov.tm.constant.PropertyConst.FILE_JSON;

@Component
public class Backup implements Runnable {

    @NotNull
    private final ScheduledExecutorService es = Executors.newSingleThreadScheduledExecutor();
    @Autowired
    private ApplicationContext context;
    @NotNull
    @Autowired
    private PropertyService propertyService;
    @NotNull
    @Autowired
    private IAuthService authService;
    @NotNull
    @Autowired
    private IProjectRecordService projectRecordService;
    @NotNull
    @Autowired
    private ISessionRecordService sessionRecordService;
    @NotNull
    @Autowired
    private ITaskRecordService taskRecordService;
    @NotNull
    @Autowired
    private IUserRecordService userRecordService;

    @NotNull
    public Domain getDomain() {
        @NotNull final Domain domain = new Domain();
        domain.setProjects(projectRecordService.findAll());
        domain.setTasks(taskRecordService.findAll());
        domain.setUsers(userRecordService.findAll());
        return domain;
    }

    public void setDomain(@Nullable final Domain domain) {
        if (domain == null) return;
        @Nullable final List<ProjectRecord> projectList = domain.getProjects();
        @Nullable final List<TaskRecord> taskList = domain.getTasks();
        @Nullable final List<UserRecord> userList = domain.getUsers();
        taskRecordService.clear();
        projectRecordService.clear();
        sessionRecordService.clear();
        userRecordService.clear();
        if (userList != null)
            userRecordService.add(userList);
        if (projectList != null)
            projectRecordService.add(projectList);
        if (taskList != null)
            taskRecordService.add(taskList);
        if (authService.isAuth())
            authService.logout();
    }

    public void init() {
        load();
        start();
    }

    @SneakyThrows
    public void load() {
        @NotNull final File file = new File(BACKUP_XML);
        if (!file.exists()) return;
        @NotNull final String xml = new String(Files.readAllBytes(Paths.get(BACKUP_XML)));
        @NotNull final ObjectMapper objectMapper = new XmlMapper();
        @Nullable final Domain domain = objectMapper.readValue(xml, Domain.class);
        setDomain(domain);
    }

    @SneakyThrows
    public void loadJson() {
        @NotNull final String json = new String(Files.readAllBytes(Paths.get(FILE_JSON)));
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final Domain domain = objectMapper.readValue(json, Domain.class);
        setDomain(domain);
    }

    @SneakyThrows
    public void run() {
        @NotNull final Domain domain = getDomain();
        @NotNull final ObjectMapper objectMapper = new XmlMapper();
        @NotNull final String xml = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);
        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(BACKUP_XML);
        fileOutputStream.write(xml.getBytes());
        fileOutputStream.flush();
        fileOutputStream.close();
    }

    @SneakyThrows
    public void saveJson() {
        @NotNull final Domain domain = getDomain();
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);
        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(FILE_JSON);
        fileOutputStream.write(json.getBytes());
        fileOutputStream.flush();
        fileOutputStream.close();
    }

    public void start() {
        es.scheduleWithFixedDelay(this, 0, propertyService.getBackupInterval(), TimeUnit.SECONDS);
    }

    public void stop() {
        es.shutdown();
    }

}
