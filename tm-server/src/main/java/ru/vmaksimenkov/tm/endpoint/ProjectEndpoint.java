package ru.vmaksimenkov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.vmaksimenkov.tm.api.endpoint.IProjectEndpoint;
import ru.vmaksimenkov.tm.api.service.dto.IProjectRecordService;
import ru.vmaksimenkov.tm.api.service.dto.ISessionRecordService;
import ru.vmaksimenkov.tm.api.service.model.IProjectTaskService;
import ru.vmaksimenkov.tm.dto.ProjectRecord;
import ru.vmaksimenkov.tm.dto.SessionRecord;
import ru.vmaksimenkov.tm.enumerated.Status;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
@Controller
public final class ProjectEndpoint extends AbstractEndpoint implements IProjectEndpoint {

    @NotNull
    @Autowired
    private IProjectRecordService projectRecordService;

    @NotNull
    @Autowired
    private ISessionRecordService sessionRecordService;

    @NotNull
    @Autowired
    private IProjectTaskService projectTaskService;

    @Override
    @WebMethod
    public void clearProject(
            @WebParam(name = "session", partName = "session") @NotNull final SessionRecord session
    ) {
        sessionRecordService.validate(session);
        projectTaskService.clearTasks(session.getUserId());
    }

    @Override
    @WebMethod
    public Long countProject(
            @WebParam(name = "session", partName = "session") @NotNull final SessionRecord session
    ) {
        sessionRecordService.validate(session);
        return projectRecordService.size(session.getUserId());
    }

    @Override
    @WebMethod
    public ProjectRecord createProject(
            @WebParam(name = "session", partName = "session") @NotNull final SessionRecord session,
            @WebParam(name = "name", partName = "name") @NotNull final String name,
            @WebParam(name = "description", partName = "description") @Nullable final String description
    ) {
        sessionRecordService.validate(session);
        return projectRecordService.add(session.getUserId(), name, description);
    }

    @Override
    @WebMethod
    public boolean existsProjectById(
            @WebParam(name = "session", partName = "session") @NotNull final SessionRecord session,
            @WebParam(name = "id", partName = "id") @NotNull final String id
    ) {
        sessionRecordService.validate(session);
        return projectRecordService.existsById(session.getUserId(), id);
    }

    @Override
    @WebMethod
    public boolean existsProjectByName(
            @WebParam(name = "session", partName = "session") @NotNull final SessionRecord session,
            @WebParam(name = "name", partName = "name") @NotNull final String name
    ) {
        sessionRecordService.validate(session);
        return projectRecordService.existsByName(session.getUserId(), name);
    }

    @Override
    @Nullable
    @WebMethod
    public List<ProjectRecord> findProjectAll(
            @WebParam(name = "session", partName = "session") @NotNull final SessionRecord session
    ) {
        sessionRecordService.validate(session);
        return projectRecordService.findAll(session.getUserId());
    }

    @Override
    @Nullable
    @WebMethod
    public ProjectRecord findProjectById(
            @WebParam(name = "session", partName = "session") @NotNull final SessionRecord session,
            @WebParam(name = "id", partName = "id") @NotNull final String id
    ) {
        sessionRecordService.validate(session);
        return projectRecordService.findById(session.getUserId(), id);
    }

    @Override
    @Nullable
    @WebMethod
    public ProjectRecord findProjectByIndex(
            @WebParam(name = "session", partName = "session") @NotNull final SessionRecord session,
            @WebParam(name = "index", partName = "index") @NotNull final Integer index
    ) {
        sessionRecordService.validate(session);
        return projectRecordService.findByIndex(session.getUserId(), index);
    }

    @Override
    @Nullable
    @WebMethod
    public ProjectRecord findProjectByName(
            @WebParam(name = "session", partName = "session") @NotNull final SessionRecord session,
            @WebParam(name = "name", partName = "name") @NotNull final String name
    ) {
        sessionRecordService.validate(session);
        return projectRecordService.findByName(session.getUserId(), name);
    }

    @Override
    @WebMethod
    public void finishProjectById(
            @WebParam(name = "session", partName = "session") @NotNull final SessionRecord session,
            @WebParam(name = "id", partName = "id") @NotNull final String id
    ) {
        sessionRecordService.validate(session);
        projectRecordService.finishProjectById(session.getUserId(), id);
    }

    @Override
    @WebMethod
    public void finishProjectByIndex(
            @WebParam(name = "session", partName = "session") @NotNull final SessionRecord session,
            @WebParam(name = "index", partName = "index") @NotNull final Integer index
    ) {
        sessionRecordService.validate(session);
        projectRecordService.finishProjectByIndex(session.getUserId(), index);
    }

    @Override
    @WebMethod
    public void finishProjectByName(
            @WebParam(name = "session", partName = "session") @NotNull final SessionRecord session,
            @WebParam(name = "name", partName = "name") @NotNull final String name
    ) {
        sessionRecordService.validate(session);
        projectRecordService.finishProjectByName(session.getUserId(), name);
    }

    @Override
    @WebMethod
    public void removeProjectById(
            @WebParam(name = "session", partName = "session") @NotNull final SessionRecord session,
            @WebParam(name = "id", partName = "id") @NotNull final String id
    ) {
        sessionRecordService.validate(session);
        projectTaskService.removeProjectById(session.getUserId(), id);
    }

    @Override
    @WebMethod
    public void removeProjectByIndex(
            @WebParam(name = "session", partName = "session") @NotNull final SessionRecord session,
            @WebParam(name = "index", partName = "index") @NotNull final Integer index
    ) {
        sessionRecordService.validate(session);
        projectTaskService.removeProjectByIndex(session.getUserId(), index);
    }

    @Override
    @WebMethod
    public void removeProjectByName(
            @WebParam(name = "session", partName = "session") @NotNull final SessionRecord session,
            @WebParam(name = "name", partName = "name") @NotNull final String name
    ) {
        sessionRecordService.validate(session);
        projectTaskService.removeProjectByName(session.getUserId(), name);
    }

    @Override
    @WebMethod
    public void setProjectStatusById(
            @WebParam(name = "session", partName = "session") @NotNull final SessionRecord session,
            @WebParam(name = "id", partName = "id") @NotNull final String id,
            @WebParam(name = "status", partName = "status") @NotNull final Status status
    ) {
        sessionRecordService.validate(session);
        projectRecordService.setProjectStatusById(session.getUserId(), id, status);
    }

    @Override
    @WebMethod
    public void setProjectStatusByIndex(
            @WebParam(name = "session", partName = "session") @NotNull final SessionRecord session,
            @WebParam(name = "index", partName = "index") @NotNull Integer index,
            @WebParam(name = "status", partName = "status") @NotNull final Status status
    ) {
        sessionRecordService.validate(session);
        projectRecordService.setProjectStatusByIndex(session.getUserId(), index, status);
    }

    @Override
    @WebMethod
    public void setProjectStatusByName(
            @WebParam(name = "session", partName = "session") @NotNull final SessionRecord session,
            @WebParam(name = "name", partName = "name") @NotNull final String name,
            @WebParam(name = "status", partName = "status") @NotNull final Status status
    ) {
        sessionRecordService.validate(session);
        projectRecordService.setProjectStatusByName(session.getUserId(), name, status);
    }

    @Override
    @WebMethod
    public void startProjectById(
            @WebParam(name = "session", partName = "session") @NotNull final SessionRecord session,
            @WebParam(name = "id", partName = "id") @NotNull final String id
    ) {
        sessionRecordService.validate(session);
        projectRecordService.startProjectById(session.getUserId(), id);
    }

    @Override
    @WebMethod
    public void startProjectByIndex(
            @WebParam(name = "session", partName = "session") @NotNull final SessionRecord session,
            @WebParam(name = "index", partName = "index") @NotNull final Integer index
    ) {
        sessionRecordService.validate(session);
        projectRecordService.startProjectByIndex(session.getUserId(), index);
    }

    @Override
    @WebMethod
    public void startProjectByName(
            @WebParam(name = "session", partName = "session") @NotNull final SessionRecord session,
            @WebParam(name = "name", partName = "name") @NotNull final String name
    ) {
        sessionRecordService.validate(session);
        projectRecordService.startProjectByName(session.getUserId(), name);
    }

    @Override
    @WebMethod
    public void updateProjectById(
            @WebParam(name = "session", partName = "session") @NotNull final SessionRecord session,
            @WebParam(name = "id", partName = "id") @NotNull final String id,
            @WebParam(name = "name", partName = "name") @NotNull final String name,
            @WebParam(name = "description", partName = "description") @NotNull final String description
    ) {
        sessionRecordService.validate(session);
        projectRecordService.updateProjectById(session.getUserId(), id, name, description);
    }

    @Override
    @WebMethod
    public void updateProjectByIndex(
            @WebParam(name = "session", partName = "session") @NotNull final SessionRecord session,
            @WebParam(name = "index", partName = "index") @NotNull final Integer index,
            @WebParam(name = "name", partName = "name") @NotNull final String name,
            @WebParam(name = "description", partName = "description") @NotNull final String description
    ) {
        sessionRecordService.validate(session);
        projectRecordService.updateProjectByIndex(session.getUserId(), index, name, description);
    }

    @Override
    @WebMethod
    public void updateProjectByName(
            @WebParam(name = "session", partName = "session") @NotNull final SessionRecord session,
            @WebParam(name = "name", partName = "name") @NotNull final String name,
            @WebParam(name = "nameNew", partName = "nameNew") @NotNull final String nameNew,
            @WebParam(name = "description", partName = "description") @NotNull final String description
    ) {
        sessionRecordService.validate(session);
        projectRecordService.updateProjectByName(session.getUserId(), name, nameNew, description);
    }

}
