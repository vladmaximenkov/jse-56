package ru.vmaksimenkov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.vmaksimenkov.tm.api.endpoint.ITaskEndpoint;
import ru.vmaksimenkov.tm.api.service.dto.IProjectTaskRecordService;
import ru.vmaksimenkov.tm.api.service.dto.ISessionRecordService;
import ru.vmaksimenkov.tm.api.service.dto.ITaskRecordService;
import ru.vmaksimenkov.tm.dto.SessionRecord;
import ru.vmaksimenkov.tm.dto.TaskRecord;
import ru.vmaksimenkov.tm.enumerated.Status;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
@Controller
public final class TaskEndpoint extends AbstractEndpoint implements ITaskEndpoint {

    @NotNull
    @Autowired
    private ISessionRecordService sessionRecordService;

    @NotNull
    @Autowired
    private IProjectTaskRecordService projectTaskRecordService;

    @NotNull
    @Autowired
    private ITaskRecordService taskRecordService;

    @Override
    @WebMethod
    public void bindTaskById(
            @WebParam(name = "session", partName = "session") @NotNull final SessionRecord session,
            @WebParam(name = "projectId", partName = "projectId") @NotNull final String projectId,
            @WebParam(name = "taskId", partName = "taskId") @NotNull final String taskId
    ) {
        sessionRecordService.validate(session);
        projectTaskRecordService.bindTaskByProjectId(session.getUserId(), projectId, taskId);
    }

    @Override
    @WebMethod
    public void clearTask(
            @WebParam(name = "session", partName = "session") @NotNull final SessionRecord session
    ) {
        sessionRecordService.validate(session);
        taskRecordService.clear(session.getUserId());
    }

    @Override
    @WebMethod
    public Long countTask(
            @WebParam(name = "session", partName = "session") @NotNull final SessionRecord session
    ) {
        sessionRecordService.validate(session);
        return taskRecordService.size(session.getUserId());
    }

    @Override
    @WebMethod
    public TaskRecord createTask(
            @WebParam(name = "session", partName = "session") @NotNull final SessionRecord session,
            @WebParam(name = "name", partName = "name") @NotNull final String name,
            @WebParam(name = "description", partName = "description") @Nullable final String description
    ) {
        sessionRecordService.validate(session);
        return taskRecordService.add(session.getUserId(), name, description);
    }

    @Override
    @WebMethod
    public boolean existsTaskById(
            @WebParam(name = "session", partName = "session") @NotNull final SessionRecord session,
            @WebParam(name = "id", partName = "id") @NotNull final String id
    ) {
        sessionRecordService.validate(session);
        return taskRecordService.existsById(session.getUserId(), id);
    }

    @Override
    @WebMethod
    public boolean existsTaskByName(
            @WebParam(name = "session", partName = "session") @NotNull final SessionRecord session,
            @WebParam(name = "name", partName = "name") @NotNull final String name
    ) {
        sessionRecordService.validate(session);
        return taskRecordService.existsByName(session.getUserId(), name);
    }

    @Override
    @Nullable
    @WebMethod
    public List<TaskRecord> findTaskAll(
            @WebParam(name = "session", partName = "session") @NotNull final SessionRecord session
    ) {
        sessionRecordService.validate(session);
        return taskRecordService.findAll(session.getUserId());
    }

    @Override
    @Nullable
    @WebMethod
    public TaskRecord findTaskById(
            @WebParam(name = "session", partName = "session") @NotNull final SessionRecord session,
            @WebParam(name = "id", partName = "id") @NotNull final String id
    ) {
        sessionRecordService.validate(session);
        return taskRecordService.findById(session.getUserId(), id);
    }

    @Override
    @Nullable
    @WebMethod
    public TaskRecord findTaskByIndex(
            @WebParam(name = "session", partName = "session") @NotNull final SessionRecord session,
            @WebParam(name = "index", partName = "index") @NotNull final Integer index
    ) {
        sessionRecordService.validate(session);
        return taskRecordService.findByIndex(session.getUserId(), index);
    }

    @Override
    @Nullable
    @WebMethod
    public TaskRecord findTaskByName(
            @WebParam(name = "session", partName = "session") @NotNull final SessionRecord session,
            @WebParam(name = "name", partName = "name") @NotNull final String name
    ) {
        sessionRecordService.validate(session);
        return taskRecordService.findByName(session.getUserId(), name);
    }

    @Override
    @Nullable
    @WebMethod
    public List<TaskRecord> findTaskByProjectId(
            @WebParam(name = "session", partName = "session") @NotNull final SessionRecord session,
            @WebParam(name = "id", partName = "id") @NotNull final String id
    ) {
        sessionRecordService.validate(session);
        return projectTaskRecordService.findAllTaskByProjectId(session.getUserId(), id);
    }

    @Override
    @WebMethod
    public void finishTaskById(
            @WebParam(name = "session", partName = "session") @NotNull final SessionRecord session,
            @WebParam(name = "id", partName = "id") @NotNull final String id
    ) {
        sessionRecordService.validate(session);
        taskRecordService.finishTaskById(session.getUserId(), id);
    }

    @Override
    @WebMethod
    public void finishTaskByIndex(
            @WebParam(name = "session", partName = "session") @NotNull final SessionRecord session,
            @WebParam(name = "index", partName = "index") @NotNull final Integer index
    ) {
        sessionRecordService.validate(session);
        taskRecordService.finishTaskByIndex(session.getUserId(), index);
    }

    @Override
    @WebMethod
    public void finishTaskByName(
            @WebParam(name = "session", partName = "session") @NotNull final SessionRecord session,
            @WebParam(name = "name", partName = "name") @NotNull final String name
    ) {
        sessionRecordService.validate(session);
        taskRecordService.finishTaskByName(session.getUserId(), name);
    }

    @Override
    @Nullable
    @WebMethod
    public String getTaskIdByIndex(
            @WebParam(name = "session", partName = "session") @NotNull final SessionRecord session,
            @WebParam(name = "index", partName = "index") @NotNull final Integer index
    ) {
        sessionRecordService.validate(session);
        return taskRecordService.getIdByIndex(session.getUserId(), index);
    }

    @Override
    @WebMethod
    public void removeTaskById(
            @WebParam(name = "session", partName = "session") @NotNull final SessionRecord session,
            @WebParam(name = "id", partName = "id") @NotNull final String id
    ) {
        sessionRecordService.validate(session);
        taskRecordService.removeById(session.getUserId(), id);
    }

    @Override
    @WebMethod
    public void removeTaskByIndex(
            @WebParam(name = "session", partName = "session") @NotNull final SessionRecord session,
            @WebParam(name = "index", partName = "index") @NotNull final Integer index
    ) {
        sessionRecordService.validate(session);
        taskRecordService.removeByIndex(session.getUserId(), index);
    }

    @Override
    @WebMethod
    public void removeTaskByName(
            @WebParam(name = "session", partName = "session") @NotNull final SessionRecord session,
            @WebParam(name = "name", partName = "name") @NotNull final String name
    ) {
        sessionRecordService.validate(session);
        taskRecordService.removeByName(session.getUserId(), name);
    }

    @Override
    @WebMethod
    public void setTaskStatusById(
            @WebParam(name = "session", partName = "session") @NotNull final SessionRecord session,
            @WebParam(name = "id", partName = "id") @NotNull final String id,
            @WebParam(name = "status", partName = "status") @NotNull final Status status
    ) {
        sessionRecordService.validate(session);
        taskRecordService.setTaskStatusById(session.getUserId(), id, status);
    }

    @Override
    @WebMethod
    public void setTaskStatusByIndex(
            @WebParam(name = "session", partName = "session") @NotNull final SessionRecord session,
            @WebParam(name = "index", partName = "index") @NotNull Integer index,
            @WebParam(name = "status", partName = "status") @NotNull final Status status
    ) {
        sessionRecordService.validate(session);
        taskRecordService.setTaskStatusByIndex(session.getUserId(), index, status);
    }

    @Override
    @WebMethod
    public void setTaskStatusByName(
            @WebParam(name = "session", partName = "session") @NotNull final SessionRecord session,
            @WebParam(name = "name", partName = "name") @NotNull final String name,
            @WebParam(name = "status", partName = "status") @NotNull final Status status
    ) {
        sessionRecordService.validate(session);
        taskRecordService.setTaskStatusByName(session.getUserId(), name, status);
    }

    @Override
    @WebMethod
    public void startTaskById(
            @WebParam(name = "session", partName = "session") @NotNull final SessionRecord session,
            @WebParam(name = "id", partName = "id") @NotNull final String id
    ) {
        sessionRecordService.validate(session);
        taskRecordService.startTaskById(session.getUserId(), id);
    }

    @Override
    @WebMethod
    public void startTaskByIndex(
            @WebParam(name = "session", partName = "session") @NotNull final SessionRecord session,
            @WebParam(name = "index", partName = "index") @NotNull final Integer index
    ) {
        sessionRecordService.validate(session);
        taskRecordService.startTaskByIndex(session.getUserId(), index);
    }

    @Override
    @WebMethod
    public void startTaskByName(
            @WebParam(name = "session", partName = "session") @NotNull final SessionRecord session,
            @WebParam(name = "name", partName = "name") @NotNull final String name
    ) {
        sessionRecordService.validate(session);
        taskRecordService.startTaskByName(session.getUserId(), name);
    }

    @Override
    @WebMethod
    public void unbindTaskById(
            @WebParam(name = "session", partName = "session") @NotNull final SessionRecord session,
            @WebParam(name = "id", partName = "id") @NotNull final String id
    ) {
        sessionRecordService.validate(session);
        projectTaskRecordService.unbindTaskFromProject(session.getUserId(), id);
    }

    @Override
    @WebMethod
    public void updateTaskById(
            @WebParam(name = "session", partName = "session") @NotNull final SessionRecord session,
            @WebParam(name = "id", partName = "id") @NotNull final String id,
            @WebParam(name = "name", partName = "name") @NotNull final String name,
            @WebParam(name = "description", partName = "description") @NotNull final String description
    ) {
        sessionRecordService.validate(session);
        taskRecordService.updateTaskById(session.getUserId(), id, name, description);
    }

    @Override
    @WebMethod
    public void updateTaskByIndex(
            @WebParam(name = "session", partName = "session") @NotNull final SessionRecord session,
            @WebParam(name = "index", partName = "index") @NotNull final Integer index,
            @WebParam(name = "name", partName = "name") @NotNull final String name,
            @WebParam(name = "description", partName = "description") @NotNull final String description
    ) {
        sessionRecordService.validate(session);
        taskRecordService.updateTaskByIndex(session.getUserId(), index, name, description);
    }

    @Override
    @WebMethod
    public void updateTaskByName(
            @WebParam(name = "session", partName = "session") @NotNull final SessionRecord session,
            @WebParam(name = "name", partName = "name") @NotNull final String name,
            @WebParam(name = "nameNew", partName = "nameNew") @NotNull final String nameNew,
            @WebParam(name = "description", partName = "description") @NotNull final String description
    ) {
        sessionRecordService.validate(session);
        taskRecordService.updateTaskByName(session.getUserId(), name, nameNew, description);
    }

}
