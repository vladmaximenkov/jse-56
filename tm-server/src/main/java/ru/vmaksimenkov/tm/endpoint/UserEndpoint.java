package ru.vmaksimenkov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.vmaksimenkov.tm.api.endpoint.IUserEndpoint;
import ru.vmaksimenkov.tm.api.service.dto.ISessionRecordService;
import ru.vmaksimenkov.tm.api.service.dto.IUserRecordService;
import ru.vmaksimenkov.tm.dto.SessionRecord;
import ru.vmaksimenkov.tm.dto.UserRecord;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
@Controller
public final class UserEndpoint extends AbstractEndpoint implements IUserEndpoint {

    @NotNull
    @Autowired
    private ISessionRecordService sessionRecordService;

    @NotNull
    @Autowired
    private IUserRecordService userRecordService;

    @Override
    @WebMethod
    public boolean existsUserByEmail(
            @WebParam(name = "email", partName = "email") @NotNull final String email
    ) {
        return userRecordService.existsByEmail(email);
    }

    @Override
    @WebMethod
    public boolean existsUserByLogin(
            @WebParam(name = "login", partName = "login") @NotNull final String login
    ) {
        return userRecordService.existsByLogin(login);
    }

    @Override
    @WebMethod
    public UserRecord registryUser(
            @WebParam(name = "login", partName = "login") @NotNull final String login,
            @WebParam(name = "password", partName = "password") @NotNull final String password,
            @WebParam(name = "email", partName = "email") @NotNull final String email
    ) {
        return userRecordService.create(login, password, email);
    }

    @Override
    @WebMethod
    public void updateUser(
            @WebParam(name = "session", partName = "session") @NotNull final SessionRecord session,
            @WebParam(name = "firstName", partName = "firstName") @NotNull final String firstName,
            @WebParam(name = "lastName", partName = "lastName") @NotNull final String lastName,
            @WebParam(name = "middleName", partName = "middleName") @NotNull final String middleName
    ) {
        sessionRecordService.validate(session);
        userRecordService.updateUser(session.getUserId(), firstName, lastName, middleName);
    }

    @Override
    @WebMethod
    public void updateUserPassword(
            @WebParam(name = "session", partName = "session") @NotNull final SessionRecord session,
            @WebParam(name = "password", partName = "password") @NotNull final String password
    ) {
        sessionRecordService.validate(session);
        userRecordService.setPassword(session.getUserId(), password);
    }

    @Override
    @Nullable
    @WebMethod
    public UserRecord viewUser(
            @WebParam(name = "session", partName = "session") @NotNull final SessionRecord session
    ) {
        sessionRecordService.validate(session);
        return userRecordService.findById(session.getUserId());
    }

}
