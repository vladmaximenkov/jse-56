package ru.vmaksimenkov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.vmaksimenkov.tm.api.service.dto.ISessionRecordService;
import ru.vmaksimenkov.tm.api.service.dto.IUserRecordService;
import ru.vmaksimenkov.tm.api.service.model.IUserService;
import ru.vmaksimenkov.tm.dto.SessionRecord;
import ru.vmaksimenkov.tm.enumerated.Role;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
@Controller
public final class AdminUserEndpoint extends AbstractEndpoint implements ru.vmaksimenkov.tm.api.endpoint.IAdminUserEndpoint {

    @NotNull
    @Autowired
    private ISessionRecordService sessionRecordService;

    @NotNull
    @Autowired
    private IUserRecordService userRecordService;

    @NotNull
    @Autowired
    private IUserService userService;

    @Override
    @WebMethod
    public void lockUserByLogin(
            @WebParam(name = "session", partName = "session") @NotNull final SessionRecord session,
            @WebParam(name = "login", partName = "login") @NotNull final String login
    ) {
        sessionRecordService.validate(session, Role.ADMIN);
        userRecordService.lockUserByLogin(login);
    }

    @Override
    @WebMethod
    public void removeUserByLogin(
            @WebParam(name = "session", partName = "session") @NotNull final SessionRecord session,
            @WebParam(name = "login", partName = "login") @NotNull final String login
    ) {
        sessionRecordService.validate(session, Role.ADMIN);
        userService.removeByLogin(login);
    }

    @Override
    @WebMethod
    public void unlockUserByLogin(
            @WebParam(name = "session", partName = "session") @NotNull final SessionRecord session,
            @WebParam(name = "login", partName = "login") @NotNull final String login
    ) {
        sessionRecordService.validate(session, Role.ADMIN);
        userRecordService.unlockUserByLogin(login);
    }

}
