package ru.vmaksimenkov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.vmaksimenkov.tm.api.service.ILoggerService;

import java.io.IOException;
import java.io.InputStream;
import java.util.logging.*;

import static ru.vmaksimenkov.tm.constant.PropertyConst.*;
import static ru.vmaksimenkov.tm.util.ValidationUtil.isEmpty;

@Service
public class LoggerService implements ILoggerService {

    @NotNull
    private final Logger commands = Logger.getLogger(COMMANDS);

    @NotNull
    private final Logger errors = Logger.getLogger(ERRORS);

    @NotNull
    private final LogManager manager = LogManager.getLogManager();

    @NotNull
    private final Logger messages = Logger.getLogger(MESSAGES);

    @NotNull
    private final Logger root = Logger.getLogger("");

    {
        init();
        registry(commands, COMMANDS_FILE, false);
        registry(errors, ERRORS_FILE, true);
        registry(messages, MESSAGES_FILE, true);
    }

    @Override
    public void command(@Nullable final String message) {
        if (isEmpty(message)) return;
        commands.info(message);
    }

    @Override
    public void debug(@Nullable final String message) {
        if (isEmpty(message)) return;
        messages.fine(message);
    }

    @Override
    public void error(@Nullable final Exception e) {
        if (e == null) return;
        errors.log(Level.SEVERE, e.getMessage(), e);
    }

    @NotNull
    private ConsoleHandler getConsoleHandler() {
        @NotNull final ConsoleHandler handler = new ConsoleHandler();
        handler.setFormatter(new Formatter() {
            @Override
            public String format(LogRecord record) {
                return record.getMessage() + "\n";
            }
        });
        return handler;
    }

    @Override
    public void info(@Nullable final String message) {
        if (isEmpty(message)) return;
        messages.info(message);
    }

    private void init() {
        try {
            @Nullable final InputStream inputStream = LoggerService.class.getResourceAsStream(LOGGER_FILE);
            manager.readConfiguration(inputStream);
        } catch (@NotNull final IOException e) {
            root.severe(e.getMessage());
        }
    }

    private void registry(@NotNull final Logger logger, @NotNull final String fileName, final boolean consoleEnabled) {
        try {
            if (consoleEnabled) logger.addHandler(getConsoleHandler());
            logger.setUseParentHandlers(false);
            logger.addHandler(new FileHandler(fileName));
        } catch (@NotNull final IOException e) {
            root.severe(e.getMessage());
        }
    }

}
