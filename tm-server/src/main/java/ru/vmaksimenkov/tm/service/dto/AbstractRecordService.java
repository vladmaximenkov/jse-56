package ru.vmaksimenkov.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import ru.vmaksimenkov.tm.api.repository.dto.IAbstractRecordRepository;
import ru.vmaksimenkov.tm.api.service.dto.IAbstractRecordService;
import ru.vmaksimenkov.tm.dto.AbstractEntityRecord;

import java.util.List;

public abstract class AbstractRecordService<E extends AbstractEntityRecord, R extends IAbstractRecordRepository<E>> implements IAbstractRecordService<E> {

    @NotNull
    @Autowired
    protected ApplicationContext context;

    @NotNull
    protected abstract R getRepository();

    @NotNull
    public List<E> findAll() {
        @NotNull final R repository = getRepository();
        try {
            repository.begin();
            return repository.findAll();
        } finally {
            repository.close();
        }
    }

    public void clear() {
        @NotNull final R repository = getRepository();
        try {
            repository.begin();
            repository.clear();
            repository.commit();
        } catch (@NotNull final Exception e) {
            repository.rollback();
            e.printStackTrace();
        } finally {
            repository.close();
        }
    }

    @Override
    @Nullable
    public E add(@Nullable final E entity) {
        @NotNull final R repository = getRepository();
        try {
            repository.begin();
            repository.add(entity);
            repository.commit();
        } catch (@NotNull final Exception e) {
            repository.rollback();
            e.printStackTrace();
        } finally {
            repository.close();
        }
        return (entity);
    }

    @Override
    public boolean existsById(@Nullable final String id) {
        @NotNull final R repository = getRepository();
        try {
            repository.begin();
            return repository.existsById(id);
        } finally {
            repository.close();
        }
    }

    @Override
    public @Nullable E findById(@NotNull final String id) {
        @NotNull final R repository = getRepository();
        try {
            repository.begin();
            return repository.findById(id);
        } finally {
            repository.close();
        }
    }

    @Override
    public void add(@Nullable final List<E> entities) {
        if (entities == null) return;
        for (E e : entities) add(e);
    }

    public void update(@NotNull final E entity) {
        @NotNull final R repository = getRepository();
        try {
            repository.begin();
            repository.update(entity);
            repository.commit();
        } catch (@NotNull final Exception e) {
            repository.rollback();
            e.printStackTrace();
        } finally {
            repository.close();
        }
    }

    @Override
    public void remove(@Nullable final E entity) {
        @NotNull final R repository = getRepository();
        try {
            repository.begin();
            repository.remove(entity);
            repository.commit();
        } catch (@NotNull final Exception e) {
            repository.rollback();
            e.printStackTrace();
        } finally {
            repository.close();
        }
    }

    @Override
    public void removeById(@Nullable final String id) {
        @NotNull final R repository = getRepository();
        try {
            repository.begin();
            repository.removeById(id);
            repository.commit();
        } catch (@NotNull final Exception e) {
            repository.rollback();
            e.printStackTrace();
        } finally {
            repository.close();
        }
    }

    @Override
    @NotNull
    public Long size() {
        @NotNull final R repository = getRepository();
        try {
            repository.begin();
            return repository.size();
        } finally {
            repository.close();
        }
    }


}