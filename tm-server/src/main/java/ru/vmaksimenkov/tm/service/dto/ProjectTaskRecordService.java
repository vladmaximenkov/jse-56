package ru.vmaksimenkov.tm.service.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import ru.vmaksimenkov.tm.api.service.dto.IProjectTaskRecordService;
import ru.vmaksimenkov.tm.dto.TaskRecord;
import ru.vmaksimenkov.tm.exception.empty.EmptyIdException;
import ru.vmaksimenkov.tm.exception.entity.ProjectNotFoundException;
import ru.vmaksimenkov.tm.exception.system.IndexIncorrectException;
import ru.vmaksimenkov.tm.repository.dto.ProjectRecordRepository;
import ru.vmaksimenkov.tm.repository.dto.TaskRecordRepository;

import java.util.List;

import static ru.vmaksimenkov.tm.util.ValidationUtil.checkIndex;
import static ru.vmaksimenkov.tm.util.ValidationUtil.isEmpty;

@Service
public class ProjectTaskRecordService implements IProjectTaskRecordService {

    @NotNull
    @Autowired
    protected ApplicationContext context;

    @NotNull
    public TaskRecordRepository getTaskRepository() {
        return context.getBean(TaskRecordRepository.class);
    }

    @NotNull
    public ProjectRecordRepository getProjectRepository() {
        return context.getBean(ProjectRecordRepository.class);
    }

    @Override
    @SneakyThrows
    public void bindTaskByProjectId(@NotNull final String userId, @Nullable final String projectId, @Nullable final String taskId) {
        @NotNull final ProjectRecordRepository projectRepository = getProjectRepository();
        @NotNull final TaskRecordRepository taskRepository = getTaskRepository();
        try {
            projectRepository.begin();
            if (!projectRepository.existsById(userId, projectId)) throw new ProjectNotFoundException();
            projectRepository.close();
            taskRepository.begin();
            taskRepository.bindTaskPyProjectId(userId, projectId, taskId);
            taskRepository.commit();
        } catch (@NotNull final Exception e) {
            taskRepository.rollback();
            throw e;
        } finally {
            taskRepository.close();
        }
    }

    @Override
    @SneakyThrows
    public void clearTasks(@NotNull final String userId) {
        @NotNull final TaskRecordRepository taskRepository = getTaskRepository();
        try {
            taskRepository.begin();
            taskRepository.removeAllBinded(userId);
            taskRepository.commit();
        } catch (@NotNull final Exception e) {
            taskRepository.rollback();
            throw e;
        } finally {
            taskRepository.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public List<TaskRecord> findAllTaskByProjectId(@NotNull final String userId, @Nullable final String projectId) {
        @NotNull final TaskRecordRepository taskRepository = getTaskRepository();
        try {
            taskRepository.begin();
            return taskRepository.findAllByProjectId(userId, projectId);
        } finally {
            taskRepository.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeProjectById(@NotNull final String userId, @Nullable final String projectId) {
        @NotNull final ProjectRecordRepository projectRepository = getProjectRepository();
        @NotNull final TaskRecordRepository taskRepository = getTaskRepository();
        try {
            projectRepository.begin();
            taskRepository.begin();
            if (!projectRepository.existsById(userId, projectId)) throw new ProjectNotFoundException();
            taskRepository.removeAllByProjectId(userId, projectId);
            projectRepository.removeById(userId, projectId);
            taskRepository.commit();
            projectRepository.commit();
        } catch (@NotNull final Exception e) {
            taskRepository.rollback();
            projectRepository.rollback();
            throw e;
        } finally {
            taskRepository.close();
            projectRepository.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeProjectByIndex(@NotNull final String userId, @NotNull final Integer projectIndex) {
        @NotNull final ProjectRecordRepository projectRepository = getProjectRepository();
        @NotNull final TaskRecordRepository taskRepository = getTaskRepository();
        try {
            projectRepository.begin();
            taskRepository.begin();
            if (!checkIndex(projectIndex, projectRepository.size(userId))) throw new IndexIncorrectException();
            @Nullable final String projectId = projectRepository.getIdByIndex(userId, projectIndex);
            if (isEmpty(projectId)) throw new EmptyIdException();
            taskRepository.removeAllByProjectId(userId, projectId);
            projectRepository.removeByIndex(userId, projectIndex);
            taskRepository.commit();
            projectRepository.commit();
        } catch (@NotNull final Exception e) {
            taskRepository.rollback();
            projectRepository.rollback();
            throw e;
        } finally {
            taskRepository.close();
            projectRepository.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeProjectByName(@NotNull final String userId, @Nullable final String projectName) {
        @NotNull final ProjectRecordRepository projectRepository = getProjectRepository();
        @NotNull final TaskRecordRepository taskRepository = getTaskRepository();
        try {
            projectRepository.begin();
            taskRepository.begin();
            @Nullable final String projectId = projectRepository.getIdByName(userId, projectName);
            if (isEmpty(projectId)) throw new EmptyIdException();
            taskRepository.removeAllByProjectId(userId, projectId);
            projectRepository.removeByName(userId, projectName);
            taskRepository.commit();
            projectRepository.commit();
        } catch (@NotNull final Exception e) {
            taskRepository.rollback();
            projectRepository.rollback();
            throw e;
        } finally {
            taskRepository.close();
            projectRepository.close();
        }
    }

    @Override
    @SneakyThrows
    public void unbindTaskFromProject(@NotNull final String userId, @Nullable final String taskId) {
        @NotNull final ProjectRecordRepository projectRepository = getProjectRepository();
        @NotNull final TaskRecordRepository taskRepository = getTaskRepository();
        try {
            projectRepository.begin();
            taskRepository.begin();
            taskRepository.unbindTaskFromProject(userId, taskId);
            taskRepository.commit();
            projectRepository.commit();
        } catch (@NotNull final Exception e) {
            taskRepository.rollback();
            projectRepository.rollback();
            throw e;
        } finally {
            taskRepository.close();
            projectRepository.close();
        }
    }

}
