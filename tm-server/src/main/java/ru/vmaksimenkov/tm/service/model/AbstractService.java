package ru.vmaksimenkov.tm.service.model;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import ru.vmaksimenkov.tm.api.repository.model.IAbstractRepository;
import ru.vmaksimenkov.tm.api.service.model.IAbstractService;
import ru.vmaksimenkov.tm.exception.entity.EntityNotFoundException;
import ru.vmaksimenkov.tm.model.AbstractEntity;

import java.util.List;

public abstract class AbstractService<E extends AbstractEntity, R extends IAbstractRepository<E>> implements IAbstractService<E> {

    @NotNull
    @Autowired
    protected ApplicationContext context;

    @NotNull
    protected abstract R getRepository();

    @Override
    @SneakyThrows
    public void add(@Nullable final E entity) {
        if (entity == null) throw new EntityNotFoundException();
        @NotNull final R repository = getRepository();
        try {
            repository.begin();
            repository.add(entity);
            repository.commit();
        } catch (@NotNull final Exception e) {
            repository.rollback();
            throw e;
        } finally {
            repository.close();
        }
    }


    @Override
    @SneakyThrows
    public void add(@Nullable final List<E> list) {
        if (list == null) return;
        for (@Nullable final E e : list) add(e);
    }

    @Override
    @SneakyThrows
    public void clear() {
        @NotNull final R repository = getRepository();
        try {
            repository.begin();
            repository.clear();
            repository.commit();
        } catch (@NotNull final Exception e) {
            repository.rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @Override
    public boolean existsById(@Nullable final String id) {
        @NotNull final R repository = getRepository();
        try {
            repository.begin();
            return repository.existsById(id);
        } finally {
            repository.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public List<E> findAll() {
        @NotNull final R repository = getRepository();
        try {
            repository.begin();
            return repository.findAll();
        } finally {
            repository.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public E findById(@NotNull final String id) {
        @NotNull final R repository = getRepository();
        try {
            repository.begin();
            return repository.findById(id);
        } finally {
            repository.close();
        }
    }

    @Override
    @SneakyThrows
    public void remove(@Nullable final E entity) {
        @NotNull final R repository = getRepository();
        try {
            repository.begin();
            repository.remove(entity);
            repository.commit();
        } catch (@NotNull final Exception e) {
            repository.rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @Override
    public void removeById(@Nullable final String id) {
        @NotNull final R repository = getRepository();
        try {
            repository.begin();
            repository.removeById(id);
            repository.commit();
        } catch (@NotNull final Exception e) {
            repository.rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @Override
    @SneakyThrows
    public Long size() {
        @NotNull final R repository = getRepository();
        try {
            repository.begin();
            return repository.size();
        } finally {
            repository.close();
        }
    }

    @Override
    @SneakyThrows
    public void update(@Nullable final E project) {
        @NotNull final R repository = getRepository();
        try {
            repository.begin();
            repository.update(project);
            repository.commit();
        } catch (@NotNull final Exception e) {
            repository.rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

}
