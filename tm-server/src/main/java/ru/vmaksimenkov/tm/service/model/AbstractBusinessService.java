package ru.vmaksimenkov.tm.service.model;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vmaksimenkov.tm.api.repository.model.IAbstractBusinessRepository;
import ru.vmaksimenkov.tm.api.service.model.IAbstractBusinessService;
import ru.vmaksimenkov.tm.model.AbstractBusinessEntity;

import java.util.List;

public abstract class AbstractBusinessService<E extends AbstractBusinessEntity, R extends IAbstractBusinessRepository<E>> extends AbstractService<E, R> implements IAbstractBusinessService<E> {

    @Override
    @SneakyThrows
    public void clear(@Nullable final String userId) {
        @NotNull final R repository = getRepository();
        try {
            repository.begin();
            repository.clear(userId);
            repository.commit();
        } catch (@NotNull final Exception e) {
            repository.rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @Override
    @SneakyThrows
    public boolean existsById(@Nullable final String userId, @Nullable final String id) {
        @NotNull final R repository = getRepository();
        try {
            repository.begin();
            return repository.existsById(userId, id);
        } finally {
            repository.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public List<E> findAll(@Nullable final String userId) {
        @NotNull final R repository = getRepository();
        try {
            repository.begin();
            return repository.findAll(userId);
        } finally {
            repository.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public E findById(@Nullable final String userId, @Nullable final String id) {
        @NotNull final R repository = getRepository();
        try {
            repository.begin();
            return repository.findById(userId, id);
        } finally {
            repository.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public E findByIndex(@Nullable final String userId, @Nullable final Integer index) {
        @NotNull final R repository = getRepository();
        try {
            repository.begin();
            return repository.findByIndex(userId, index);
        } finally {
            repository.close();
        }
    }

    @Nullable
    @Override
    public String getIdByIndex(@Nullable final String userId, @Nullable final Integer index) {
        @NotNull final R repository = getRepository();
        try {
            repository.begin();
            return repository.getIdByIndex(userId, index);
        } finally {
            repository.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeById(@Nullable final String userId, @Nullable final String id) {
        @NotNull final R repository = getRepository();
        try {
            repository.begin();
            repository.removeById(userId, id);
            repository.commit();
        } catch (@NotNull final Exception e) {
            repository.rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @Override
    public void removeByIndex(@Nullable final String userId, @Nullable final Integer index) {
        @NotNull final R repository = getRepository();
        try {
            repository.begin();
            repository.removeByIndex(userId, index);
            repository.commit();
        } catch (@NotNull final Exception e) {
            repository.rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public Long size(@Nullable final String userId) {
        @NotNull final R repository = getRepository();
        try {
            repository.begin();
            return repository.size(userId);
        } finally {
            repository.close();
        }
    }

}
