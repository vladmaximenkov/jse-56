package ru.vmaksimenkov.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vmaksimenkov.tm.api.repository.dto.IAbstractBusinessRecordRepository;
import ru.vmaksimenkov.tm.api.service.dto.IAbstractBusinessRecordService;
import ru.vmaksimenkov.tm.dto.AbstractBusinessEntityRecord;

import java.util.List;

public abstract class AbstractBusinessRecordService<E extends AbstractBusinessEntityRecord, R extends IAbstractBusinessRecordRepository<E>> extends AbstractRecordService<E, R> implements IAbstractBusinessRecordService<E> {

    @Override
    public void clear(@Nullable final String userId) {
        @NotNull final R repository = getRepository();
        try {
            repository.begin();
            repository.clear(userId);
            repository.commit();
        } catch (@NotNull final Exception e) {
            repository.rollback();
            e.printStackTrace();
        } finally {
            repository.close();
        }
    }

    @Override
    public boolean existsById(@Nullable final String userId, @Nullable final String id) {
        @NotNull final R repository = getRepository();
        try {
            repository.begin();
            return repository.existsById(userId, id);
        } finally {
            repository.close();
        }
    }

    @Override
    public @NotNull List<E> findAll(@Nullable final String userId) {
        @NotNull final R repository = getRepository();
        try {
            repository.begin();
            return repository.findAll(userId);
        } finally {
            repository.close();
        }
    }

    @Override
    public @Nullable E findById(@Nullable final String userId, @Nullable final String id) {
        @NotNull final R repository = getRepository();
        try {
            repository.begin();
            return repository.findById(userId, id);
        } finally {
            repository.close();
        }
    }

    @Override
    public @Nullable E findByIndex(@Nullable final String userId, @Nullable final Integer index) {
        @NotNull final R repository = getRepository();
        try {
            repository.begin();
            return repository.findByIndex(userId, index);
        } finally {
            repository.close();
        }
    }

    @Override
    public @Nullable String getIdByIndex(@Nullable final String userId, @Nullable final Integer index) {
        @NotNull final R repository = getRepository();
        try {
            repository.begin();
            return repository.getIdByIndex(userId, index);
        } finally {
            repository.close();
        }
    }

    @Override
    public void removeById(@Nullable final String userId, @Nullable final String id) {
        @NotNull final R repository = getRepository();
        try {
            repository.begin();
            repository.removeById(userId, id);
            repository.commit();
        } catch (@NotNull final Exception e) {
            repository.rollback();
            e.printStackTrace();
        } finally {
            repository.close();
        }
    }

    @Override
    public void removeByIndex(@Nullable final String userId, @Nullable final Integer index) {
        @NotNull final R repository = getRepository();
        try {
            repository.begin();
            repository.removeByIndex(userId, index);
            repository.commit();
        } catch (@NotNull final Exception e) {
            repository.rollback();
            e.printStackTrace();
        } finally {
            repository.close();
        }
    }

    @Override
    public @NotNull Long size(@Nullable final String userId) {
        @NotNull final R repository = getRepository();
        try {
            repository.begin();
            return repository.size(userId);
        } finally {
            repository.close();
        }
    }
}