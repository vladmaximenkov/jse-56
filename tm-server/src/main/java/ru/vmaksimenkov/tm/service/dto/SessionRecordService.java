package ru.vmaksimenkov.tm.service.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.vmaksimenkov.tm.api.repository.dto.ISessionRecordRepository;
import ru.vmaksimenkov.tm.api.repository.dto.IUserRecordRepository;
import ru.vmaksimenkov.tm.api.service.IPropertyService;
import ru.vmaksimenkov.tm.api.service.dto.ISessionRecordService;
import ru.vmaksimenkov.tm.dto.SessionRecord;
import ru.vmaksimenkov.tm.dto.UserRecord;
import ru.vmaksimenkov.tm.enumerated.Role;
import ru.vmaksimenkov.tm.exception.user.AccessDeniedException;
import ru.vmaksimenkov.tm.repository.dto.SessionRecordRepository;
import ru.vmaksimenkov.tm.repository.dto.UserRecordRepository;
import ru.vmaksimenkov.tm.util.HashUtil;
import ru.vmaksimenkov.tm.util.SignatureUtil;

import java.util.List;

import static ru.vmaksimenkov.tm.util.ValidationUtil.isEmpty;

@Service
public final class SessionRecordService extends AbstractBusinessRecordService<SessionRecord, SessionRecordRepository> implements ISessionRecordService {

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @NotNull
    @Override
    protected SessionRecordRepository getRepository() {
        return context.getBean(SessionRecordRepository.class);
    }

    @NotNull
    public IUserRecordRepository getUserRepository() {
        return context.getBean(UserRecordRepository.class);
    }

    @Override
    @SneakyThrows
    public boolean checkDataAccess(@Nullable final String login, @Nullable final String password) {
        if (isEmpty(login) || isEmpty(password)) return false;
        @NotNull final IUserRecordRepository repository = getUserRepository();
        try {
            repository.begin();
            @Nullable final UserRecord user = repository.findByLogin(login);
            if (user == null) return false;
            if (user.isLocked()) return false;
            @Nullable final String passwordHash = HashUtil.salt(propertyService, password);
            if (isEmpty(passwordHash)) return false;
            return passwordHash.equals(user.getPasswordHash());
        } finally {
            repository.close();
        }
    }

    @Override
    public void close(@NotNull final SessionRecord session) {
        validate(session);
        @NotNull final ISessionRecordRepository repository = getRepository();
        try {
            repository.begin();
            repository.remove(session);
            repository.commit();
        } catch (@NotNull final Exception e) {
            repository.rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @Override
    @SneakyThrows
    public void closeAll(@NotNull final SessionRecord session) {
        validate(session);
        @NotNull final ISessionRecordRepository repository = getRepository();
        try {
            repository.begin();
            repository.clear(session.getUserId());
            repository.commit();
        } catch (@NotNull final Exception e) {
            repository.rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public List<SessionRecord> getListSession(@NotNull final SessionRecord session) {
        validate(session);
        @NotNull final ISessionRecordRepository repository = getRepository();
        try {
            repository.begin();
            return repository.findAll(session.getUserId());
        } finally {
            repository.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public UserRecord getUser(@NotNull final SessionRecord session) {
        @NotNull final String userId = getUserId(session);
        @NotNull final IUserRecordRepository repository = getUserRepository();
        try {
            repository.begin();
            return repository.findById(userId);
        } finally {
            repository.close();
        }
    }

    @NotNull
    @Override
    public String getUserId(@NotNull final SessionRecord session) {
        validate(session);
        return session.getUserId();
    }

    @Override
    public boolean isValid(@NotNull final SessionRecord session) {
        try {
            validate(session);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public SessionRecord open(@Nullable final String login, @Nullable final String password) {
        final boolean check = checkDataAccess(login, password);
        if (!check || isEmpty(login)) return null;
        @NotNull final IUserRecordRepository repository = getUserRepository();
        try {
            repository.begin();
            @Nullable final UserRecord user = repository.findByLogin(login);
            if (user == null) return null;
            @NotNull final SessionRecord session = new SessionRecord();
            session.setUserId(user.getId());
            session.setTimestamp(System.currentTimeMillis());
            add(session);
            repository.commit();
            return sign(session);
        } catch (@NotNull final Exception e) {
            repository.rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @Nullable
    @Override
    public SessionRecord sign(@Nullable final SessionRecord session) {
        if (session == null) return null;
        session.setSignature(null);
        @Nullable final String salt = propertyService.getSessionSalt();
        final int cycle = propertyService.getSessionCycle();
        @Nullable final String signature = SignatureUtil.sign(session, salt, cycle);
        session.setSignature(signature);
        update(session);
        return session;
    }

    @Override
    @SneakyThrows
    public void validate(@NotNull final SessionRecord session, @Nullable final Role role) {
        if (role == null) throw new AccessDeniedException();
        validate(session);
        @Nullable final String userId = session.getUserId();
        @NotNull final IUserRecordRepository repository = getUserRepository();
        try {
            repository.begin();
            @Nullable final UserRecord user = repository.findById(userId);
            if (user == null) throw new AccessDeniedException();
            if (user.getRole() == null) throw new AccessDeniedException();
            if (!role.equals(user.getRole())) throw new AccessDeniedException();
        } finally {
            repository.close();
        }
    }

    @Override
    @SneakyThrows
    public void validate(@Nullable final SessionRecord session) {
        if (session == null) throw new AccessDeniedException();
        if (isEmpty(session.getSignature())) throw new AccessDeniedException();
        if (isEmpty(session.getUserId())) throw new AccessDeniedException();
        @Nullable final SessionRecord temp = session.clone();
        if (temp == null) throw new AccessDeniedException();
        @NotNull final String signatureSource = session.getSignature();
        @Nullable final SessionRecord sessionSign = sign(temp);
        if (sessionSign == null) throw new AccessDeniedException();
        @Nullable final String signatureTarget = sessionSign.getSignature();
        final boolean check = signatureSource.equals(signatureTarget);
        if (!check) throw new AccessDeniedException();
        @NotNull final ISessionRecordRepository repository = getRepository();
        try {
            repository.begin();
            if (!repository.existsById(session.getId())) throw new AccessDeniedException();
        } finally {
            repository.close();
        }
    }
}
