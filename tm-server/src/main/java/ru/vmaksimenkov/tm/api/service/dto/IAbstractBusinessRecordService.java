package ru.vmaksimenkov.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vmaksimenkov.tm.dto.AbstractBusinessEntityRecord;

import java.util.List;

public interface IAbstractBusinessRecordService<E extends AbstractBusinessEntityRecord> extends IAbstractRecordService<E> {

    void clear(@Nullable String userId);

    boolean existsById(@Nullable String userId, @Nullable String id);

    @NotNull
    List<E> findAll(@Nullable String userId);


    @Nullable
    E findById(@Nullable String userId, @Nullable String id);

    @Nullable
    E findByIndex(@Nullable String userId, @Nullable Integer index);

    @Nullable
    String getIdByIndex(@Nullable String userId, @Nullable Integer index);

    void removeById(@Nullable String userId, @Nullable String id);

    void removeByIndex(@Nullable String userId, @Nullable Integer index);

    @NotNull Long size(@Nullable String userId);

}
