package ru.vmaksimenkov.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vmaksimenkov.tm.enumerated.Role;
import ru.vmaksimenkov.tm.model.Session;
import ru.vmaksimenkov.tm.model.User;

import java.util.List;

public interface ISessionService extends IAbstractBusinessService<Session> {

    boolean checkDataAccess(@Nullable String login, @Nullable String password);

    void close(@NotNull Session session);

    void closeAll(@NotNull Session session);

    @Nullable List<Session> getListSession(@NotNull Session session);

    @Nullable User getUser(@NotNull Session session);

    @NotNull String getUserId(@NotNull Session session);

    boolean isValid(@NotNull Session session);

    @Nullable Session open(@Nullable String login, @Nullable String password);

    @Nullable Session sign(@Nullable Session session);

    void validate(@NotNull Session session, @Nullable Role role);

    void validate(@Nullable Session session);

}
