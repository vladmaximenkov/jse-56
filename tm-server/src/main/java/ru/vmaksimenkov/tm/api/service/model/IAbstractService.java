package ru.vmaksimenkov.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vmaksimenkov.tm.model.AbstractEntity;

import java.util.List;

public interface IAbstractService<E extends AbstractEntity> {

    void add(@NotNull E entity);

    void add(@Nullable List<E> list);

    void clear();

    boolean existsById(@Nullable String id);

    @Nullable
    List<E> findAll();

    @Nullable
    E findById(@NotNull String id);

    void remove(@NotNull E entity);

    void removeById(@Nullable String id);

    @Nullable
    Long size();

    void update(@NotNull E project);
}
