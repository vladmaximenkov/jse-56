package ru.vmaksimenkov.tm.repository.dto;

import org.hibernate.jpa.QueryHints;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.vmaksimenkov.tm.api.repository.dto.ITaskRecordRepository;
import ru.vmaksimenkov.tm.dto.TaskRecord;

import java.util.List;

@Repository
@Scope("prototype")
public class TaskRecordRepository extends AbstractBusinessRecordRepository<TaskRecord> implements ITaskRecordRepository {

    @Override
    @Nullable
    public TaskRecord findById(@NotNull final String id) {
        return getEntity(entityManager.createQuery("FROM TaskRecord WHERE id = :id", TaskRecord.class)
                .setParameter("id", id)
                .setMaxResults(1));
    }

    @Override
    public void removeById(@Nullable final String id) {
        entityManager.createQuery("DELETE TaskRecord where id = :id")
                .setParameter("id", id).executeUpdate();
    }

    @Override
    public void bindTaskPyProjectId(@Nullable final String userId, @Nullable final String projectId, @Nullable final String taskId) {
        @Nullable final TaskRecord task = findById(userId, taskId);
        if (task == null) return;
        task.setProjectId(projectId);
        update(task);
    }

    @Override
    public void clear(@Nullable final String userId) {
        entityManager.createQuery("DELETE TaskRecord where userId = :userId")
                .setParameter("userId", userId).executeUpdate();
    }

    @Override
    public void clear() {
        entityManager.createQuery("DELETE TaskRecord").executeUpdate();
    }

    @Override
    public boolean existsById(@Nullable final String userId, @Nullable final String id) {
        return entityManager.createQuery("SELECT COUNT(*) FROM TaskRecord WHERE userId = :userId AND id = :id", Long.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .getSingleResult() > 0;
    }

    @Override
    public boolean existsById(@Nullable final String id) {
        return entityManager.createQuery("SELECT COUNT(*) FROM TaskRecord WHERE id = :id", Long.class)
                .setParameter("id", id)
                .getSingleResult() > 0;
    }

    @Override
    public boolean existsByName(@Nullable final String userId, @Nullable final String name) {
        return entityManager.createQuery("SELECT COUNT(*) FROM TaskRecord WHERE userId = :userId AND name = :name", Long.class)
                .setParameter("userId", userId)
                .setParameter("name", name)
                .getSingleResult() > 0;
    }

    @Override
    public boolean existsByProjectId(@Nullable final String userId, @Nullable final String projectId) {
        return entityManager.createQuery("SELECT COUNT(*) FROM TaskRecord WHERE userId = :userId AND projectId = :projectId", Long.class)
                .setParameter("userId", userId)
                .setParameter("projectId", projectId)
                .getSingleResult() > 0;
    }

    @NotNull
    @Override
    public List<TaskRecord> findAll(@Nullable final String userId) {
        return entityManager.createQuery("FROM TaskRecord WHERE userId = :userId", TaskRecord.class)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .setParameter("userId", userId)
                .getResultList();
    }

    @NotNull
    @Override
    public List<TaskRecord> findAll() {
        return entityManager.createQuery("FROM TaskRecord", TaskRecord.class)
                .setHint(QueryHints.HINT_CACHEABLE, true).getResultList();
    }

    @Nullable
    @Override
    public List<TaskRecord> findAllByProjectId(@Nullable final String userId, @Nullable final String projectId) {
        return entityManager.createQuery("FROM TaskRecord WHERE userId = :userId AND projectId = :projectId", TaskRecord.class)
                .setParameter("userId", userId)
                .setParameter("projectId", projectId)
                .getResultList();
    }

    @Nullable
    @Override
    public TaskRecord findById(@Nullable final String userId, @Nullable final String id) {
        return getEntity(entityManager.createQuery("FROM TaskRecord WHERE userId = :userId AND id = :id", TaskRecord.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .setMaxResults(1));
    }

    @Nullable
    @Override
    public TaskRecord findByName(@Nullable final String userId, @Nullable final String name) {
        return getEntity(entityManager.createQuery("FROM TaskRecord WHERE userId = :userId AND name = :name", TaskRecord.class)
                .setParameter("userId", userId)
                .setParameter("name", name)
                .setMaxResults(1));
    }

    @Override
    public void removeAllBinded(@Nullable final String userId) {
        entityManager.createQuery("DELETE TaskRecord WHERE userId = :userId AND projectId IS NOT NULL").executeUpdate();
    }

    @Override
    public void removeAllByProjectId(@Nullable final String userId, @Nullable final String projectId) {
        entityManager.createQuery("DELETE TaskRecord WHERE userId = :userId AND projectId = :projectId")
                .setParameter("userId", userId)
                .setParameter("projectId", projectId)
                .executeUpdate();
    }

    @Override
    public void removeOneByName(@Nullable final String userId, @Nullable final String name) {
        entityManager.createQuery("DELETE TaskRecord WHERE userId = :userId AND name = :name")
                .setParameter("userId", userId)
                .setParameter("name", name)
                .executeUpdate();
    }

    @NotNull
    @Override
    public Long size(@Nullable final String userId) {
        return entityManager.createQuery("SELECT COUNT(*) FROM TaskRecord WHERE userId = :userId", Long.class)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .setParameter("userId", userId)
                .setMaxResults(1)
                .getSingleResult();
    }

    @Override
    public @NotNull Long size() {
        return entityManager.createQuery("SELECT COUNT(*) FROM TaskRecord", Long.class)
                .setHint(QueryHints.HINT_CACHEABLE, true).setMaxResults(1).getSingleResult();
    }

    @Override
    public void unbindTaskFromProject(@Nullable final String userId, @Nullable final String id) {
        @Nullable final TaskRecord task = findById(userId, id);
        if (task == null) return;
        task.setProjectId(null);
        update(task);
    }

}
