package ru.vmaksimenkov.tm.repository.dto;

import org.hibernate.jpa.QueryHints;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.vmaksimenkov.tm.api.repository.dto.IProjectRecordRepository;
import ru.vmaksimenkov.tm.dto.ProjectRecord;

import java.util.List;

@Repository
@Scope("prototype")
public class ProjectRecordRepository extends AbstractBusinessRecordRepository<ProjectRecord> implements IProjectRecordRepository {

    @Override
    @Nullable
    public ProjectRecord findById(@NotNull final String id) {
        return getEntity(entityManager.createQuery("FROM ProjectRecord WHERE id = :id", ProjectRecord.class)
                .setParameter("id", id)
                .setMaxResults(1));
    }

    @Override
    public void removeById(@Nullable final String id) {
        entityManager.createQuery("DELETE ProjectRecord where id = :id")
                .setParameter("id", id).executeUpdate();
    }

    @Override
    public void clear(@Nullable final String userId) {
        entityManager.createQuery("DELETE ProjectRecord where userId = :userId")
                .setParameter("userId", userId).executeUpdate();
    }

    @Override
    public void clear() {
        entityManager.createQuery("DELETE ProjectRecord").executeUpdate();
    }

    @Override
    public boolean existsById(@Nullable final String userId, @Nullable final String id) {
        return entityManager.createQuery("SELECT COUNT(*) FROM ProjectRecord WHERE userId = :userId AND id = :id", Long.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .getSingleResult() > 0;
    }

    @Override
    public boolean existsById(@Nullable final String id) {
        return entityManager.createQuery("SELECT COUNT(*) FROM ProjectRecord WHERE id = :id", Long.class)
                .setParameter("id", id)
                .getSingleResult() > 0;
    }

    @Override
    public boolean existsByName(@Nullable final String userId, @Nullable final String name) {
        return entityManager.createQuery("SELECT COUNT(*) FROM ProjectRecord WHERE userId = :userId AND name = :name", Long.class)
                .setParameter("userId", userId)
                .setParameter("name", name)
                .getSingleResult() > 0;
    }

    @NotNull
    @Override
    public List<ProjectRecord> findAll(@Nullable final String userId) {
        return entityManager.createQuery("FROM ProjectRecord WHERE userId = :userId", ProjectRecord.class)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .setParameter("userId", userId)
                .getResultList();
    }

    @NotNull
    @Override
    public List<ProjectRecord> findAll() {
        return entityManager.createQuery("FROM ProjectRecord", ProjectRecord.class)
                .setHint(QueryHints.HINT_CACHEABLE, true).getResultList();
    }

    @Nullable
    @Override
    public ProjectRecord findById(@Nullable final String userId, @Nullable final String id) {
        return getEntity(entityManager.createQuery("FROM ProjectRecord WHERE userId = :userId AND id = :id", ProjectRecord.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .setMaxResults(1));
    }

    @Nullable
    @Override
    public ProjectRecord findByName(@Nullable final String userId, @Nullable final String name) {
        return getEntity(entityManager.createQuery("FROM ProjectRecord WHERE userId = :userId AND name = :name", ProjectRecord.class)
                .setParameter("userId", userId)
                .setParameter("name", name)
                .setMaxResults(1));
    }

    @Nullable
    @Override
    public String getIdByName(@Nullable final String userId, @Nullable final String name) {
        return entityManager.createQuery("SELECT id FROM ProjectRecord WHERE userId = :userId AND name = :name", String.class)
                .setParameter("userId", userId)
                .setParameter("name", name)
                .setMaxResults(1)
                .getSingleResult();
    }

    @Override
    public void removeByName(@Nullable final String userId, @Nullable final String name) {
        entityManager.createQuery("DELETE FROM ProjectRecord WHERE userId = :userId AND name = :name")
                .setParameter("userId", userId)
                .setParameter("name", name).executeUpdate();
    }

    @NotNull
    @Override
    public Long size(@Nullable final String userId) {
        return entityManager.createQuery("SELECT COUNT(*) FROM ProjectRecord WHERE userId = :userId", Long.class)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .setParameter("userId", userId)
                .setMaxResults(1)
                .getSingleResult();
    }

    @Override
    public @NotNull Long size() {
        return entityManager.createQuery("SELECT COUNT(*) FROM ProjectRecord", Long.class)
                .setHint(QueryHints.HINT_CACHEABLE, true).setMaxResults(1).getSingleResult();
    }

}
