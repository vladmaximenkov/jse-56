package ru.vmaksimenkov.tm.repository.dto;

import org.hibernate.jpa.QueryHints;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.vmaksimenkov.tm.api.repository.dto.ISessionRecordRepository;
import ru.vmaksimenkov.tm.dto.SessionRecord;

import java.util.List;

@Repository
@Scope("prototype")
public class SessionRecordRepository extends AbstractBusinessRecordRepository<SessionRecord> implements ISessionRecordRepository {

    @Override
    @Nullable
    public SessionRecord findById(@NotNull final String id) {
        return getEntity(entityManager.createQuery("FROM SessionRecord WHERE id = :id", SessionRecord.class)
                .setParameter("id", id)
                .setMaxResults(1));
    }

    @Override
    public void removeById(@Nullable final String id) {
        entityManager.createQuery("DELETE SessionRecord where id = :id")
                .setParameter("id", id).executeUpdate();
    }

    @Override
    public void clear(@Nullable final String userId) {
        entityManager.createQuery("DELETE SessionRecord where userId = :userId")
                .setParameter("userId", userId).executeUpdate();
    }

    @Override
    public void clear() {
        entityManager.createQuery("DELETE SessionRecord").executeUpdate();
    }

    @Override
    public boolean existsById(@Nullable final String id) {
        return entityManager.createQuery("SELECT COUNT(*) FROM SessionRecord WHERE id = :id", Long.class)
                .setParameter("id", id)
                .getSingleResult() > 0;
    }

    @Override
    public boolean existsById(@Nullable final String userId, @Nullable final String id) {
        return entityManager.createQuery("SELECT COUNT(*) FROM SessionRecord WHERE userId = :userId AND id = :id", Long.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .getSingleResult() > 0;
    }

    @NotNull
    @Override
    public List<SessionRecord> findAll(@Nullable final String userId) {
        return entityManager.createQuery("FROM SessionRecord WHERE userId = :userId", SessionRecord.class)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .setParameter("userId", userId)
                .getResultList();
    }

    @NotNull
    @Override
    public List<SessionRecord> findAll() {
        return entityManager.createQuery("FROM SessionRecord", SessionRecord.class)
                .setHint(QueryHints.HINT_CACHEABLE, true).getResultList();
    }

    @Nullable
    @Override
    public SessionRecord findById(@Nullable final String userId, @Nullable final String id) {
        return getEntity(entityManager.createQuery("FROM SessionRecord WHERE userId = :userId AND id = :id", SessionRecord.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .setMaxResults(1));
    }

    @NotNull
    @Override
    public Long size(@Nullable final String userId) {
        return entityManager.createQuery("SELECT COUNT(*) FROM SessionRecord WHERE userId = :userId", Long.class)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .setParameter("userId", userId)
                .setMaxResults(1)
                .getSingleResult();
    }

    @Override
    public @NotNull Long size() {
        return entityManager.createQuery("SELECT COUNT(*) FROM SessionRecord", Long.class)
                .setHint(QueryHints.HINT_CACHEABLE, true).setMaxResults(1).getSingleResult();
    }

}
