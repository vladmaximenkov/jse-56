package ru.vmaksimenkov.tm.repository.dto;

import org.jetbrains.annotations.Nullable;
import ru.vmaksimenkov.tm.api.repository.dto.IAbstractBusinessRecordRepository;
import ru.vmaksimenkov.tm.dto.AbstractBusinessEntityRecord;

import java.util.List;
import java.util.Objects;

public abstract class AbstractBusinessRecordRepository<E extends AbstractBusinessEntityRecord> extends AbstractRecordRepository<E> implements IAbstractBusinessRecordRepository<E> {

    @Nullable
    public E findByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (index == null) return null;
        @Nullable final List<E> list = findAll(userId);
        if (list == null || list.isEmpty()) return null;
        return list.get(index - 1);
    }

    @Nullable
    @Override
    public String getIdByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (index == null) return null;
        return Objects.requireNonNull(findAll(userId)).get(index - 1).getId();
    }

    @Override
    public void removeById(@Nullable final String userId, @Nullable final String id) {
        remove(findById(userId, id));
    }

    @Override
    public void removeByIndex(@Nullable final String userId, @Nullable final Integer index) {
        remove(findByIndex(userId, index));
    }

}