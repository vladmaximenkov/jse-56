package ru.vmaksimenkov.tm.repository.model;

import org.hibernate.jpa.QueryHints;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.vmaksimenkov.tm.api.repository.model.IProjectRepository;
import ru.vmaksimenkov.tm.api.repository.model.ITaskRepository;
import ru.vmaksimenkov.tm.model.Project;
import ru.vmaksimenkov.tm.model.Task;

import java.util.List;

@Repository
@Scope("prototype")
public class TaskRepository extends AbstractBusinessRepository<Task> implements ITaskRepository {

    @NotNull
    @Autowired
    private IProjectRepository projectRepository;

    @Override
    public void bindTaskPyProjectId(@Nullable final String userId, @Nullable final String projectId, @Nullable final String taskId) {
        @Nullable final Task task = findById(userId, taskId);
        if (task == null || projectId == null) return;
        @Nullable final Project project = projectRepository.findById(projectId);
        task.setProject(project);
        update(task);
    }

    @Override
    public void clear(@Nullable final String userId) {
        entityManager.createQuery("DELETE Task where user.id = :userId")
                .setParameter("userId", userId).executeUpdate();
    }

    @Override
    public void clear() {
        entityManager.createQuery("DELETE Task").executeUpdate();
    }

    @Override
    public boolean existsById(@Nullable final String userId, @Nullable final String id) {
        return entityManager.createQuery("SELECT COUNT(*) FROM Task WHERE user.id = :userId AND id = :id", Long.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .getSingleResult() > 0;
    }

    @Override
    public boolean existsById(@Nullable final String id) {
        return entityManager.createQuery("SELECT COUNT(*) FROM Task WHERE id = :id", Long.class)
                .setParameter("id", id)
                .getSingleResult() > 0;
    }

    @Override
    public boolean existsByName(@Nullable final String userId, @Nullable final String name) {
        return entityManager.createQuery("SELECT COUNT(*) FROM Task WHERE user.id = :userId AND name = :name", Long.class)
                .setParameter("userId", userId)
                .setParameter("name", name)
                .getSingleResult() > 0;
    }

    @Override
    public boolean existsByProjectId(@Nullable final String userId, @Nullable final String projectId) {
        return entityManager.createQuery("SELECT COUNT(*) FROM Task WHERE user.id = :userId AND project.id = :projectId", Long.class)
                .setParameter("userId", userId)
                .setParameter("projectId", projectId)
                .getSingleResult() > 0;
    }

    @Nullable
    @Override
    public List<Task> findAll(@Nullable final String userId) {
        return entityManager.createQuery("FROM Task WHERE user.id = :userId", Task.class)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .setParameter("userId", userId)
                .getResultList();
    }

    @NotNull
    @Override
    public List<Task> findAll() {
        return entityManager.createQuery("FROM Task", Task.class)
                .setHint(QueryHints.HINT_CACHEABLE, true).getResultList();
    }

    @Nullable
    @Override
    public List<Task> findAllByProjectId(@Nullable final String userId, @Nullable final String projectId) {
        return entityManager.createQuery("FROM Task WHERE user.id = :userId AND project.id = :projectId", Task.class)
                .setParameter("userId", userId)
                .setParameter("projectId", projectId)
                .getResultList();
    }

    @Nullable
    @Override
    public Task findById(@Nullable final String userId, @Nullable final String id) {
        return getEntity(entityManager.createQuery("FROM Task WHERE user.id = :userId AND id = :id", Task.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .setMaxResults(1));
    }

    @Nullable
    @Override
    public Task findById(@Nullable final String id) {
        return getEntity(entityManager.createQuery("FROM Task WHERE user.id = :userId AND id = :id", Task.class)
                .setParameter("id", id)
                .setMaxResults(1));
    }

    @Nullable
    @Override
    public Task findByName(@Nullable final String userId, @Nullable final String name) {
        return getEntity(entityManager.createQuery("FROM Task WHERE user.id = :userId AND name = :name", Task.class)
                .setParameter("userId", userId)
                .setParameter("name", name)
                .setMaxResults(1));
    }

    @Override
    public void removeAllBinded(@Nullable final String userId) {
        entityManager.createQuery("DELETE Task WHERE user.id = :userId AND project.id IS NOT NULL").executeUpdate();
    }

    @Override
    public void removeAllByProjectId(@Nullable final String userId, @Nullable final String projectId) {
        entityManager.createQuery("DELETE Task WHERE user.id = :userId AND project.id = :projectId")
                .setParameter("userId", userId)
                .setParameter("projectId", projectId)
                .executeUpdate();
    }

    @Override
    public void removeOneByName(@Nullable final String userId, @Nullable final String name) {
        entityManager.createQuery("DELETE Task WHERE user.id = :userId AND name = :name")
                .setParameter("userId", userId)
                .setParameter("name", name)
                .executeUpdate();
    }

    @NotNull
    @Override
    public Long size(@Nullable final String userId) {
        return entityManager.createQuery("SELECT COUNT(*) FROM Task WHERE user.id = :userId", Long.class)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .setParameter("userId", userId)
                .setMaxResults(1)
                .getSingleResult();
    }

    @NotNull
    @Override
    public Long size() {
        return entityManager.createQuery("SELECT COUNT(*) FROM Task", Long.class)
                .setHint(QueryHints.HINT_CACHEABLE, true).setMaxResults(1).getSingleResult();
    }

    @Override
    public void unbindTaskFromProject(@Nullable final String userId, @Nullable final String id) {
        @Nullable final Task task = findById(userId, id);
        if (task == null) return;
        task.setProject(null);
        update(task);
    }

}
