package ru.vmaksimenkov.tm.configuration;

import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Environment;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import ru.vmaksimenkov.tm.api.service.IPropertyService;
import ru.vmaksimenkov.tm.dto.ProjectRecord;
import ru.vmaksimenkov.tm.dto.SessionRecord;
import ru.vmaksimenkov.tm.dto.TaskRecord;
import ru.vmaksimenkov.tm.dto.UserRecord;
import ru.vmaksimenkov.tm.model.Project;
import ru.vmaksimenkov.tm.model.Session;
import ru.vmaksimenkov.tm.model.Task;
import ru.vmaksimenkov.tm.model.User;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.HashMap;
import java.util.Map;

import static ru.vmaksimenkov.tm.constant.PropertyConst.HIBERNATE_LITEMEMBER_KEY;

@Configuration
@ComponentScan("ru.vmaksimenkov.tm")
public class ServerConfiguration {

    @Bean
    @NotNull
    public EntityManagerFactory entityManagerFactory(@NotNull final IPropertyService propertyService) {
        @NotNull final Map<String, String> settings = new HashMap<>();
        settings.put(Environment.DRIVER, propertyService.getJdbcDriver());
        settings.put(Environment.URL, propertyService.getJdbcUrl());
        settings.put(Environment.USER, propertyService.getJdbcUsername());
        settings.put(Environment.PASS, propertyService.getJdbcPassword());

        settings.put(Environment.DIALECT, propertyService.getJdbcDialect());
        settings.put(Environment.HBM2DDL_AUTO, propertyService.getJdbcHBM2DDL());
        settings.put(Environment.SHOW_SQL, propertyService.getJdbcShowSql());
        settings.put(Environment.FORMAT_SQL, propertyService.getFormatSQL());

        settings.put(Environment.USE_SECOND_LEVEL_CACHE, propertyService.getSecondLevelCash());
        settings.put(Environment.USE_QUERY_CACHE, propertyService.getQueryCache());
        settings.put(Environment.USE_MINIMAL_PUTS, propertyService.getMinimalPuts());
        settings.put(Environment.CACHE_REGION_PREFIX, propertyService.getRegionPrefix());
        settings.put(Environment.CACHE_PROVIDER_CONFIG, propertyService.getCacheProvider());
        settings.put(Environment.CACHE_REGION_FACTORY, propertyService.getFactoryClass());
        settings.put(HIBERNATE_LITEMEMBER_KEY, propertyService.getLiteMember());

        @NotNull final StandardServiceRegistryBuilder registryBuilder = new StandardServiceRegistryBuilder();
        registryBuilder.applySettings(settings);
        @NotNull final StandardServiceRegistry registry = registryBuilder.build();
        @NotNull final MetadataSources sources = new MetadataSources(registry);

        sources.addAnnotatedClass(ProjectRecord.class);
        sources.addAnnotatedClass(Project.class);

        sources.addAnnotatedClass(TaskRecord.class);
        sources.addAnnotatedClass(Task.class);

        sources.addAnnotatedClass(SessionRecord.class);
        sources.addAnnotatedClass(Session.class);

        sources.addAnnotatedClass(UserRecord.class);
        sources.addAnnotatedClass(User.class);

        @NotNull final Metadata metadata = sources.getMetadataBuilder().build();
        return metadata.getSessionFactoryBuilder().build();
    }

    @Bean
    @NotNull
    @Scope("prototype")
    public EntityManager entityManager(@NotNull final EntityManagerFactory entityManagerFactory) {
        return entityManagerFactory.createEntityManager();
    }

}
