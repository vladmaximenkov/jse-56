package ru.vmaksimenkov.tm;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vmaksimenkov.tm.api.repository.dto.IProjectRecordRepository;
import ru.vmaksimenkov.tm.api.repository.dto.ISessionRecordRepository;
import ru.vmaksimenkov.tm.api.repository.dto.ITaskRecordRepository;
import ru.vmaksimenkov.tm.api.repository.dto.IUserRecordRepository;
import ru.vmaksimenkov.tm.api.service.dto.IProjectRecordService;
import ru.vmaksimenkov.tm.api.service.dto.ISessionRecordService;
import ru.vmaksimenkov.tm.api.service.dto.ITaskRecordService;
import ru.vmaksimenkov.tm.api.service.dto.IUserRecordService;
import ru.vmaksimenkov.tm.dto.ProjectRecord;
import ru.vmaksimenkov.tm.dto.SessionRecord;
import ru.vmaksimenkov.tm.dto.TaskRecord;
import ru.vmaksimenkov.tm.dto.UserRecord;
import ru.vmaksimenkov.tm.repository.dto.ProjectRecordRepository;
import ru.vmaksimenkov.tm.repository.dto.SessionRecordRepository;
import ru.vmaksimenkov.tm.repository.dto.TaskRecordRepository;
import ru.vmaksimenkov.tm.repository.dto.UserRecordRepository;
import ru.vmaksimenkov.tm.service.dto.ProjectRecordService;
import ru.vmaksimenkov.tm.service.dto.SessionRecordService;
import ru.vmaksimenkov.tm.service.dto.TaskRecordService;
import ru.vmaksimenkov.tm.service.dto.UserRecordService;

public class AbstractRecordTest extends AbstractTest {

    @NotNull
    protected static final IProjectRecordRepository PROJECT_REPOSITORY = CONTEXT.getBean(ProjectRecordRepository.class);

    @NotNull
    protected static final IProjectRecordService PROJECT_SERVICE = CONTEXT.getBean(ProjectRecordService.class);

    @NotNull
    protected static final ISessionRecordRepository SESSION_REPOSITORY = CONTEXT.getBean(SessionRecordRepository.class);

    @NotNull
    protected static final ISessionRecordService SESSION_SERVICE = CONTEXT.getBean(SessionRecordService.class);

    @NotNull
    protected static final ITaskRecordRepository TASK_REPOSITORY = CONTEXT.getBean(TaskRecordRepository.class);

    @NotNull
    protected static final ITaskRecordService TASK_SERVICE = CONTEXT.getBean(TaskRecordService.class);

    @NotNull
    protected static final IUserRecordRepository USER_REPOSITORY = CONTEXT.getBean(UserRecordRepository.class);

    @NotNull
    protected static final IUserRecordService USER_SERVICE = CONTEXT.getBean(UserRecordService.class);

    @Nullable
    protected static ProjectRecord TEST_PROJECT = new ProjectRecord();

    @Nullable
    protected static SessionRecord TEST_SESSION;

    @Nullable
    protected static TaskRecord TEST_TASK = new TaskRecord();

    @NotNull
    protected static UserRecord TEST_USER = new UserRecord();

}
