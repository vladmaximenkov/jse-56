package ru.vmaksimenkov.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.vmaksimenkov.tm.AbstractRecordTest;
import ru.vmaksimenkov.tm.dto.UserRecord;
import ru.vmaksimenkov.tm.marker.DBCategory;
import ru.vmaksimenkov.tm.util.HashUtil;

public class UserRecordRepositoryTest extends AbstractRecordTest {

    @After
    public void after() {
        USER_REPOSITORY.clear();
        USER_REPOSITORY.commit();
    }

    @Before
    public void before() {
        USER_REPOSITORY.begin();
        TEST_USER = AUTH_SERVICE.registry(TEST_USER_NAME, TEST_USER_PASSWORD, TEST_USER_EMAIL);
        TEST_USER_ID = TEST_USER.getId();
    }

    @Test
    @Category(DBCategory.class)
    public void existByEmail() {
        Assert.assertTrue(USER_REPOSITORY.existsByEmail(TEST_USER_EMAIL));
    }

    @Test
    @Category(DBCategory.class)
    public void passwordChange() {
        @NotNull final String password = HashUtil.salt(PROPERTY_SERVICE, TEST_PASSWORD);
        TEST_USER.setPasswordHash(password);
        USER_REPOSITORY.update(TEST_USER);
        @Nullable final UserRecord user = USER_REPOSITORY.findById(TEST_USER_ID);
        Assert.assertNotNull(user);
        @Nullable final String passwordRepo = user.getPasswordHash();
        Assert.assertNotNull(passwordRepo);
        Assert.assertEquals(password, passwordRepo);
    }

    @Test
    @Category(DBCategory.class)
    public void removeByLogin() {
        USER_REPOSITORY.removeByLogin(TEST_USER_NAME);
        Assert.assertTrue(USER_REPOSITORY.findAll().isEmpty());
    }

}
