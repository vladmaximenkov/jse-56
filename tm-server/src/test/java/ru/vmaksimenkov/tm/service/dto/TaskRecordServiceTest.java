package ru.vmaksimenkov.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.vmaksimenkov.tm.AbstractRecordTest;
import ru.vmaksimenkov.tm.dto.TaskRecord;
import ru.vmaksimenkov.tm.enumerated.Status;
import ru.vmaksimenkov.tm.marker.DBCategory;

import java.util.ArrayList;
import java.util.List;

public class TaskRecordServiceTest extends AbstractRecordTest {

    @Test
    @Category(DBCategory.class)
    public void addAll() {
        initTwo();
        Assert.assertEquals(2, TASK_SERVICE.findAll().size());
    }

    @After
    public void after() {
        TASK_SERVICE.clear();
        PROJECT_SERVICE.clear();
        USER_SERVICE.clear();
    }

    @Before
    public void before() {
        TEST_USER = AUTH_SERVICE.registry(TEST_USER_NAME, TEST_USER_PASSWORD, TEST_USER_EMAIL);
        TEST_USER_ID = TEST_USER.getId();
        @NotNull final TaskRecord task = new TaskRecord();
        TEST_TASK_ID = task.getId();
        task.setName(TEST_TASK_NAME);
        task.setUserId(TEST_USER_ID);
        TASK_SERVICE.add(task);
        TEST_TASK = TASK_SERVICE.findById(TEST_USER_ID, task.getId());
    }

    @Test
    @Category(DBCategory.class)
    public void clear() {
        TASK_SERVICE.clear(TEST_USER_ID);
        Assert.assertTrue(TASK_SERVICE.findAll(TEST_USER_ID).isEmpty());
        TASK_SERVICE.clear();
        Assert.assertTrue(TASK_SERVICE.findAll().isEmpty());
    }

    @Test
    @Category(DBCategory.class)
    public void create() {
        Assert.assertNotNull(TEST_TASK);
        Assert.assertNotNull(TEST_TASK.getId());
        Assert.assertNotNull(TEST_TASK.getName());
        Assert.assertEquals(TEST_TASK_NAME, TEST_TASK.getName());
        Assert.assertNotNull(TASK_SERVICE.findAll(TEST_USER_ID));
    }

    @Test
    @Category(DBCategory.class)
    public void existsById() {
        Assert.assertTrue(TASK_SERVICE.existsById(TEST_USER_ID, TEST_TASK_ID));
    }

    @Test
    @Category(DBCategory.class)
    public void existsByName() {
        Assert.assertTrue(TASK_SERVICE.existsByName(TEST_USER_ID, TEST_TASK_NAME));
    }

    @Test
    @Category(DBCategory.class)
    public void findByName() {
        @Nullable final TaskRecord taskRecord = TASK_SERVICE.findByName(TEST_USER_ID, TEST_TASK_NAME);
        Assert.assertNotNull(taskRecord);
        Assert.assertEquals(TEST_TASK_ID, taskRecord.getId());
    }

    private void initTwo() {
        TASK_SERVICE.clear(TEST_USER_ID);
        @NotNull final TaskRecord task1 = new TaskRecord(TEST_TASK_NAME);
        task1.setUserId(TEST_USER_ID);
        task1.setStatus(Status.IN_PROGRESS);
        @NotNull final TaskRecord task2 = new TaskRecord(TEST_TASK_NAME_TWO);
        task2.setUserId(TEST_USER_ID);
        task2.setStatus(Status.COMPLETE);
        @NotNull final List<TaskRecord> list = new ArrayList<>();
        list.add(task1);
        list.add(task2);
        TASK_SERVICE.add(list);
    }

    @Test
    @Category(DBCategory.class)
    public void remove() {
        Assert.assertNotNull(TEST_TASK);
        TASK_SERVICE.remove(TEST_TASK);
        Assert.assertNull(TASK_SERVICE.findById(TEST_TASK.getId()));
        Assert.assertTrue(TASK_SERVICE.findAll(TEST_USER_ID).isEmpty());
    }

    @Test
    @Category(DBCategory.class)
    public void removeByName() {
        initTwo();
        TASK_SERVICE.removeByName(TEST_USER_ID, TEST_TASK_NAME);
        Assert.assertEquals(TEST_TASK_NAME_TWO, TASK_SERVICE.findAll(TEST_USER_ID).get(0).getName());
    }

}
