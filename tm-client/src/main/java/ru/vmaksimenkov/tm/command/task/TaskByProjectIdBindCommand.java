package ru.vmaksimenkov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.vmaksimenkov.tm.exception.entity.TaskNotFoundException;
import ru.vmaksimenkov.tm.util.TerminalUtil;

@Component
public final class TaskByProjectIdBindCommand extends AbstractTaskCommand {

    @Nullable
    @Override
    public String commandArg() {
        return null;
    }

    @NotNull
    @Override
    public String commandDescription() {
        return "Bind task by project id";
    }

    @NotNull
    @Override
    public String commandName() {
        return "task-bind";
    }

    @Override
    public void execute() {
        System.out.println("[BIND TASK TO PROJECT BY ID]");
        if (taskEndpoint.countTask(sessionService.getSession()) < 1)
            throw new TaskNotFoundException();
        System.out.println("ENTER TASK INDEX:");
        @Nullable final String taskId = taskEndpoint.getTaskIdByIndex(sessionService.getSession(), TerminalUtil.nextNumber());
        if (taskId == null) return;
        System.out.println("ENTER PROJECT ID:");
        taskEndpoint.bindTaskById(sessionService.getSession(), TerminalUtil.nextLine(), taskId);
    }

}
