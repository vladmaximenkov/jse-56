package ru.vmaksimenkov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.vmaksimenkov.tm.command.AbstractCommand;
import ru.vmaksimenkov.tm.component.Bootstrap;

import java.util.Collection;

@Component
public final class CommandsCommand extends AbstractCommand {

    @NotNull
    @Autowired
    protected Bootstrap bootstrap;

    @NotNull
    @Override
    public String commandArg() {
        return "-c";
    }

    @NotNull
    @Override
    public String commandDescription() {
        return "Show all commands";
    }

    @NotNull
    @Override
    public String commandName() {
        return "commands";
    }

    @Override
    public void execute() {
        System.out.println("[COMMANDS]");
        @Nullable final Collection<AbstractCommand> commands = bootstrap.getCommands().values();
        commands.forEach(e -> System.out.println(e.commandName()));
    }

}
