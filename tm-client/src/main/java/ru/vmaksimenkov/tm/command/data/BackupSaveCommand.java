package ru.vmaksimenkov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.vmaksimenkov.tm.endpoint.Role;

@Component
public final class BackupSaveCommand extends AbstractDataCommand {

    @Nullable
    @Override
    public String commandArg() {
        return null;
    }

    @Nullable
    @Override
    public String commandDescription() {
        return "Save backup to XML";
    }

    @NotNull
    @Override
    public String commandName() {
        return "backup-save";
    }

    @Override
    public @Nullable Role[] commandRoles() {
        return null;
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[BACKUP SAVE]");
        adminEndpoint.saveBackup(sessionService.getSession());
    }

}
