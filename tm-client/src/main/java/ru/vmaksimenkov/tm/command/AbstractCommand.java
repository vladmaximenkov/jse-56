package ru.vmaksimenkov.tm.command;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vmaksimenkov.tm.endpoint.Role;

@NoArgsConstructor
public abstract class AbstractCommand {

    @Nullable
    public abstract String commandArg();

    @Nullable
    public abstract String commandDescription();

    @NotNull
    public abstract String commandName();

    @Nullable
    public Role[] commandRoles() {
        return null;
    }

    public abstract void execute();

}