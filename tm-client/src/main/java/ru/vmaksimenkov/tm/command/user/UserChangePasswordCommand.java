package ru.vmaksimenkov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.vmaksimenkov.tm.endpoint.UserEndpoint;
import ru.vmaksimenkov.tm.util.TerminalUtil;

@Component
public final class UserChangePasswordCommand extends AbstractUserCommand {

    @NotNull
    @Autowired
    protected UserEndpoint userEndpoint;

    @Nullable
    @Override
    public String commandArg() {
        return null;
    }

    @NotNull
    @Override
    public String commandDescription() {
        return "Change user password";
    }

    @NotNull
    @Override
    public String commandName() {
        return "user-change-password";
    }

    @Override
    public void execute() {
        System.out.println("[CHANGE PASSWORD]");
        System.out.println("ENTER NEW PASSWORD:");
        final String password = TerminalUtil.nextLine();
        userEndpoint.updateUserPassword(sessionService.getSession(), password);
    }

}
