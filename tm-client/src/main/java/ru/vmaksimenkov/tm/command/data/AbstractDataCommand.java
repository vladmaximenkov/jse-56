package ru.vmaksimenkov.tm.command.data;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import ru.vmaksimenkov.tm.command.AbstractCommand;
import ru.vmaksimenkov.tm.endpoint.AdminEndpoint;
import ru.vmaksimenkov.tm.service.SessionService;

public abstract class AbstractDataCommand extends AbstractCommand {

    @NotNull
    @Autowired
    protected SessionService sessionService;

    @NotNull
    @Autowired
    protected AdminEndpoint adminEndpoint;

}
