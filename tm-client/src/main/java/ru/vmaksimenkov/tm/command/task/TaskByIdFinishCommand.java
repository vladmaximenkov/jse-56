package ru.vmaksimenkov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.vmaksimenkov.tm.util.TerminalUtil;

@Component
public final class TaskByIdFinishCommand extends AbstractTaskCommand {

    @Nullable
    @Override
    public String commandArg() {
        return null;
    }

    @NotNull
    @Override
    public String commandDescription() {
        return "Finish task by id";
    }

    @NotNull
    @Override
    public String commandName() {
        return "task-finish-by-id";
    }

    @Override
    public void execute() {
        System.out.println("[FINISH TASK]");
        System.out.println("ENTER ID:");
        taskEndpoint.finishTaskById(sessionService.getSession(), TerminalUtil.nextLine());
    }

}
