package ru.vmaksimenkov.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.vmaksimenkov.tm.api.service.IPropertyService;

import java.io.InputStream;
import java.util.Properties;

@Service
public class PropertyService implements IPropertyService {

    @NotNull
    private static final String APPLICATION_VERSION_DEFAULT = "";
    @NotNull
    private static final String APPLICATION_VERSION_KEY = "application.version";
    @NotNull
    private static final String FILE_NAME = "application.properties";
    @NotNull
    private static final String PASSWORD_ITERATION_DEFAULT = "1";
    @NotNull
    private static final String PASSWORD_ITERATION_KEY = "password.iteration";
    @NotNull
    private static final String PASSWORD_SECRET_DEFAULT = "";
    @NotNull
    private static final String PASSWORD_SECRET_KEY = "password.secret";
    @NotNull
    private static final String SCANNER_INTERVAL_DEFAULT = "5";
    @NotNull
    private static final String SCANNER_INTERVAL_KEY = "scanner.interval";
    private static final int SCANNER_INTERVAL_MIN = 1;
    @NotNull
    private final Properties properties = new Properties();

    @SneakyThrows
    public PropertyService() {
        @Nullable final InputStream inputStream = ClassLoader.getSystemResourceAsStream(FILE_NAME);
        if (inputStream == null) return;
        properties.load(inputStream);
        inputStream.close();
    }

    @NotNull
    @Override
    public String getApplicationVersion() {
        @Nullable final String systemProperty = System.getProperty(APPLICATION_VERSION_KEY);
        if (systemProperty != null) return systemProperty;
        @Nullable final String environmentProperty = System.getenv(APPLICATION_VERSION_KEY);
        if (environmentProperty != null) return environmentProperty;
        return properties.getProperty(APPLICATION_VERSION_KEY, APPLICATION_VERSION_DEFAULT);
    }

    @NotNull
    @Override
    public Integer getPasswordIteration() {
        @Nullable final String systemProperty = System.getProperty(PASSWORD_ITERATION_KEY);
        if (systemProperty != null) return Integer.parseInt(systemProperty);
        @Nullable final String environmentProperty = System.getenv(PASSWORD_ITERATION_KEY);
        if (environmentProperty != null) return Integer.parseInt(environmentProperty);
        return Integer.parseInt(
                properties.getProperty(PASSWORD_ITERATION_KEY, PASSWORD_ITERATION_DEFAULT)
        );
    }

    @NotNull
    @Override
    public String getPasswordSecret() {
        @Nullable final String systemProperty = System.getProperty(PASSWORD_SECRET_KEY);
        if (systemProperty != null) return systemProperty;
        @Nullable final String environmentProperty = System.getenv(PASSWORD_SECRET_KEY);
        if (environmentProperty != null) return environmentProperty;
        return properties.getProperty(PASSWORD_SECRET_KEY, PASSWORD_SECRET_DEFAULT);
    }

    @Override
    public int getScannerInterval() {
        @Nullable final String systemProperty = System.getProperty(SCANNER_INTERVAL_KEY);
        if (systemProperty != null) return Math.max(Integer.parseInt(systemProperty), SCANNER_INTERVAL_MIN);
        @Nullable final String environmentProperty = System.getenv(SCANNER_INTERVAL_KEY);
        if (environmentProperty != null) return Math.max(Integer.parseInt(environmentProperty), SCANNER_INTERVAL_MIN);
        final int interval = Integer.parseInt(properties.getProperty(SCANNER_INTERVAL_KEY, SCANNER_INTERVAL_DEFAULT));
        return Math.max(interval, SCANNER_INTERVAL_MIN);
    }

}
