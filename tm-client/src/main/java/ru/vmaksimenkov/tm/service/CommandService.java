package ru.vmaksimenkov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.vmaksimenkov.tm.api.service.ICommandService;
import ru.vmaksimenkov.tm.command.AbstractCommand;
import ru.vmaksimenkov.tm.component.Bootstrap;

import java.util.Collection;
import java.util.stream.Collectors;

import static ru.vmaksimenkov.tm.util.ValidationUtil.isEmpty;

@Service
public class CommandService implements ICommandService {

    @NotNull
    @Autowired
    private Bootstrap bootstrap;

    @Override
    @Nullable
    public Collection<AbstractCommand> getArguments() {
        return bootstrap.getCommands().values().stream()
                .filter(e -> !isEmpty(e.commandArg()))
                .collect(Collectors.toList());
    }

}
