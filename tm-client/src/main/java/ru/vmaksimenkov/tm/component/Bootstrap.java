package ru.vmaksimenkov.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.vmaksimenkov.tm.command.AbstractCommand;
import ru.vmaksimenkov.tm.exception.system.UnknownCommandException;
import ru.vmaksimenkov.tm.service.LoggerService;
import ru.vmaksimenkov.tm.util.SystemUtil;
import ru.vmaksimenkov.tm.util.TerminalUtil;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.LinkedHashMap;
import java.util.Map;

import static ru.vmaksimenkov.tm.constant.Constant.PID_FILENAME;
import static ru.vmaksimenkov.tm.util.ValidationUtil.isEmpty;

@Getter
@Component
public final class Bootstrap {

    @NotNull
    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();
    @NotNull
    @Autowired
    protected FileScanner fileScanner;
    @NotNull
    @Autowired
    protected LoggerService loggerService;
    @Nullable
    @Autowired
    private AbstractCommand[] abstractCommands;

    private void init() {
        initCommands(abstractCommands);
        initPID();
        fileScanner.init();
    }

    private void initCommands(@Nullable final AbstractCommand[] commands) {
        if (commands == null) return;
        for (@Nullable final AbstractCommand command : commands) {
            if (command == null) continue;
            this.commands.put(command.commandName(), command);
        }
    }

    @SneakyThrows
    public void initPID() {
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(PID_FILENAME), pid.getBytes());
        @NotNull final File file = new File(PID_FILENAME);
        file.deleteOnExit();
    }

    public void parseArg(@Nullable final String arg) {
        if (isEmpty(arg)) return;
        @Nullable final AbstractCommand command = getCommandByArg(arg);
        if (command == null) return;
        command.execute();
    }

    public boolean parseArgs(@Nullable final String[] args) {
        if (args == null || args.length < 1) return false;
        @Nullable final String arg = args[0];
        parseArg(arg);
        return true;
    }

    public void parseCommand(@Nullable final String cmd) {
        if (isEmpty(cmd)) return;
        @Nullable final AbstractCommand command = commands.get(cmd);
        if (command == null) throw new UnknownCommandException(cmd);
        command.execute();
    }

    public void run(@Nullable final String... args) {
        loggerService.debug("Debug mode");
        loggerService.info("** WELCOME TO TASK MANAGER **");
        if (parseArgs(args)) System.exit(0);
        init();
        //noinspection InfiniteLoopStatement
        while (true) {
            System.out.println("Enter command: ");
            @Nullable final String command = TerminalUtil.nextLine();
            loggerService.command(command);
            try {
                parseCommand(command);
                System.err.println("[OK]");
            } catch (@NotNull final Exception e) {
                loggerService.error(e);
                System.err.println("[FAIL]");
            }
        }
    }

    @Nullable
    private AbstractCommand getCommandByArg(@Nullable final String arg) {
        if (isEmpty(arg)) return null;
        for (@NotNull final AbstractCommand command : commands.values()) {
            if (arg.equals(command.commandArg())) return command;
        }
        return null;
    }

}
