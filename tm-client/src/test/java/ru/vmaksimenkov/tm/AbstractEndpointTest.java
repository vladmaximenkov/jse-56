package ru.vmaksimenkov.tm;

import org.jetbrains.annotations.NotNull;
import ru.vmaksimenkov.tm.component.Bootstrap;
import ru.vmaksimenkov.tm.endpoint.*;

public abstract class AbstractEndpointTest {

    @NotNull
    protected static final String ADMIN_USER_NAME = "admin";
    @NotNull
    protected static final String ADMIN_USER_PASSWORD = "admin";
    @NotNull
    protected static final Bootstrap BOOTSTRAP = new Bootstrap();
    @NotNull
    protected static final SessionEndpoint SESSION_ENDPOINT = new SessionEndpointService().getSessionEndpointPort();
    @NotNull
    protected static final String TEST_USER_NAME = "test";
    @NotNull
    protected static final String TEST_USER_PASSWORD = "test";
    @NotNull
    protected static final AdminEndpoint ADMIN_ENDPOINT = new AdminEndpointService().getAdminEndpointPort();
    @NotNull
    protected static final AdminUserEndpoint ADMIN_USER_ENDPOINT = new AdminUserEndpointService().getAdminUserEndpointPort();
    @NotNull
    protected static final ProjectEndpoint PROJECT_ENDPOINT = new ProjectEndpointService().getProjectEndpointPort();
    @NotNull
    protected static final TaskEndpoint TASK_ENDPOINT = new TaskEndpointService().getTaskEndpointPort();
    @NotNull
    protected static final UserEndpoint USER_ENDPOINT = new UserEndpointService().getUserEndpointPort();
    @NotNull
    protected static SessionRecord SESSION = SESSION_ENDPOINT.openSession(TEST_USER_NAME, TEST_USER_PASSWORD);

}
